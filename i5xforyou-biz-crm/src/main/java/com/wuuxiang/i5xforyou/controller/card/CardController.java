package com.wuuxiang.i5xforyou.controller.card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.service.MemberCardsService;
import com.wuuxiang.i5xforyou.vo.CrmCard;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/** 
 * @ClassName: CardController <br/>
 * crm会员卡相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:11:19
 *
 */
@RestController
@RefreshScope
@Api(value = "卡相关", tags="卡相关", protocols="https")
public class CardController extends BaseController {

	@Autowired
	private MemberCardsService memberCardsService;

	/**
	 * getPrimaryCard(获取用户主卡)
	 * @author Mobile Web Group-daizhen
	 * @date 2018年3月27日 下午4:25:41
	 *
	 * @param mpId
	 * @param openId
	 * @return
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:获取主卡", notes = "说明:获取用户主卡信息，如果存在主卡则返回主卡卡号")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true,dataType = "String"),
        @ApiImplicitParam(name = "openId", value = "OPENID", required = true, dataType = "String")
	})
	@ApiResponses({@ApiResponse(code=400,message="请求参数没填好"),
        @ApiResponse(code=404,message="请求路径没有或页面跳转路径不对")
	})
	@RequestMapping(value = "/card/getprimarycard", method = RequestMethod.GET)
	public JsonPackage getPrimaryCard(String mpId, String openId) {
		JsonPackage jsonPackage = new JsonPackage();
		// 获取用戶主卡
		CrmCard card = memberCardsService.getPrimaryCard(mpId, openId);
		if (card != null) {
			jsonPackage.setResult(card.getCardNo());
		}

		return jsonPackage;
	}

}
