package com.wuuxiang.i5xforyou.controller.company;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;

/** 
 * @ClassName: CompanyController <br/>
 * crm集团相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:15:29
 *
 */
@RestController
@RequestMapping("/company")
public class CompanyController extends BaseController {

}
