package com.wuuxiang.i5xforyou.controller.coupon;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;

/** 
 * @ClassName: CouponController <br/>
 * crm优惠卷相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:12:58
 *
 */
@RestController
@RequestMapping("/coupon")
public class CouponController extends BaseController {

}
