package com.wuuxiang.i5xforyou.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.domain.Results;
import com.wuuxiang.i5xforyou.service.CommonService;
import com.wuuxiang.i5xforyou.vo.CrmVersion;

/** 
 * @ClassName: CommonController <br/>
 * crm共通 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 上午9:11:32
 *
 */
@RestController
public class CommonController extends BaseController {
	@Autowired
	private CommonService commonService;
	
	/**
	 * getCrmVersion <br/>
	 * 获取crm的版本号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:15:19
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/common/crmversion", method = RequestMethod.GET)
	public JsonPackage getCrmVersion(String mpId) {
		JsonPackage jsonPackage = new JsonPackage();
		//查询crm版本
		CrmVersion crmVersion = commonService.getCrmVersion(mpId);
		//转为jsonPackage
		if (CrmVersion.CRM7 == crmVersion) {
			jsonPackage.setResult("7");
		} else if (CrmVersion.CRM6 == crmVersion) {
			jsonPackage.setResult("6");
		} else {
			//获取crm版本失败
			jsonPackage.setStatus(-1);
			jsonPackage.setResult("UNKNOWN");
		}
		return jsonPackage;
	}
	
	/**
	 * getCrm7Code <br/>
	 * 获取crm7的流水号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:15:37
	 *
	 * @param mpId
	 * @param openId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/common/crm7code", method = RequestMethod.GET)
	public JsonPackage getCrm7Code(String mpId, String openId) {
		JsonPackage jsonPackage = new JsonPackage();
		//获取crm7流水号
		ResultMsg resultMsg = commonService.getCrm7Code(mpId, openId);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * getShops <br/>
	 * 获取门店信息的列表 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午4:23:04
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/common/shops", method = RequestMethod.GET)
	public JsonPackage getShops(String mpId) {
		JsonPackage jsonPackage = new JsonPackage();
		//获取门店信息的列表
		jsonPackage.setResult(commonService.getShops(mpId));
		return jsonPackage;
	}
	
	
	
}
