package com.wuuxiang.i5xforyou.controller.activity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;

/** 
 * @ClassName: ActivityController <br/>
 * crm活动相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:14:12
 *
 */
@RestController
@RequestMapping("/activity")
public class ActivityController extends BaseController {

}
