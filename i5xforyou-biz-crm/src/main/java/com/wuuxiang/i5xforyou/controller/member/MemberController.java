package com.wuuxiang.i5xforyou.controller.member;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;

/** 
 * @ClassName: MemberController <br/>
 * crm会员相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:09:12
 *
 */
@RestController
@RequestMapping("/member")
public class MemberController extends BaseController {

}
