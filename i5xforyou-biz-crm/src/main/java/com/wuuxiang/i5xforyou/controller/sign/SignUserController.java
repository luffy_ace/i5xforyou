package com.wuuxiang.i5xforyou.controller.sign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.domain.Results;
import com.wuuxiang.i5xforyou.service.UserService;
import com.wuuxiang.i5xforyou.vo.CrmCards;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RefreshScope
@Api(value = "用户验证", tags="用户验证", protocols="https")
public class SignUserController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	/**
	 * getSMSVerifyCode(发送短信验证码)
	 * @author Mobile Web Group-daizhen
	 * @date 2018年3月27日 下午4:26:27
	 *
	 * @param mobile
	 * @param leftTime
	 * @param mpId
	 * @return
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:发送短信验证码", notes = "说明:发送短信验证码到制定手机号")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "mobile", value = "手机号码", required = true,dataType = "String"),
		@ApiImplicitParam(name = "leftTime", value = "剩余时间", required = true,dataType = "String"),
        @ApiImplicitParam(name = "mpId", value = "MPID", required = true, dataType = "String")}
	)
	@RequestMapping(value = "/sign/getsmsauthcode", method = RequestMethod.POST)
	public JsonPackage getSMSVerifyCode(String mobile, String leftTime, String mpId) {
		JsonPackage jsonPackage = new JsonPackage();
		// 获取短信验证码
	   	ResultMsg resultMsg = userService.getSmsVerifyCode(mobile, leftTime, mpId);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			log.error("调用发送短信验证码接口失败，入参：mobile="+mobile+",leftTime="+leftTime+",mpId="+mpId+"，出参："+JSON.toJSONString(jsonPackage));
			return jsonPackage;
		}
		//成功
		jsonPackage.setMessage(resultMsg.getMsg());
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * signDinerCard(认证用户)
	 * @author Mobile Web Group-daizhen
	 * @date 2018年3月27日 下午4:26:55
	 *
	 * @param nickName
	 * @param sex
	 * @param verifyCode
	 * @param mpId
	 * @param openId
	 * @param phoneNo
	 * @param marketActivityId
	 * @return
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:用户认证", notes = "说明:认证用户，并为用户申请一张会员卡")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "nickName", value = "昵称", required = true,dataType = "String"),
		@ApiImplicitParam(name = "sex", value = "性别", required = true,dataType = "String"),
		@ApiImplicitParam(name = "verifyCode", value = "短信验证码", required = true,dataType = "String"),
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true,dataType = "String"),
		@ApiImplicitParam(name = "openId", value = "OPENID", required = true,dataType = "String"),
		@ApiImplicitParam(name = "phoneNo", value = "电话号码", required = true,dataType = "String"),
        @ApiImplicitParam(name = "marketActivityId", value = "活动ID", required = true, dataType = "String")}
	)
	 @RequestMapping(value = "/sign/signuser", method = RequestMethod.POST)
	    public JsonPackage signDinerCard(String nickName, String sex, String verifyCode, String mpId, String openId, String phoneNo, String marketActivityId) {
	    	JsonPackage jsonPackage = new JsonPackage();
	    	ResultMsg resultMsg = userService.signUserDiner(nickName, sex, verifyCode, mpId, openId, phoneNo, marketActivityId);
	    	if (Results.ERROR.equals(resultMsg.getState())){
	    		log.error("用户认证:" + resultMsg.getException().getMessage());
	    		jsonPackage.setStatus(-1);
	    		jsonPackage.setMessage(resultMsg.getException().getMessage());
	    		log.error("调用用户认证接口失败，入参：nickName="+nickName+",sex="+sex+",verifyCode="+verifyCode+",mpId="+mpId+",openId="+openId+",phoneNo="+phoneNo+",marketActivityId="+marketActivityId+"，出参："+JSON.toJSONString(jsonPackage));
	    		return jsonPackage;
	    	}
	    	//获取到申请会员卡的信息，如果会员卡申请成功，则通过result返回会员卡信息，失败则不返回
	    	CrmCards crmCards = (CrmCards) resultMsg.getMsgEntity();
	    	if(crmCards != null && !crmCards.getCrmCardInfo().isEmpty()) {
	    		jsonPackage.setResult(crmCards);
	    	}
	    	return jsonPackage;
	 }
	
}
