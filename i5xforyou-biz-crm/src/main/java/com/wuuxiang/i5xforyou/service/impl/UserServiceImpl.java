package com.wuuxiang.i5xforyou.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.domain.Results;
import com.wuuxiang.i5xforyou.common.service.impl.BaseServiceImpl;
import com.wuuxiang.i5xforyou.service.MemberCardsService;
import com.wuuxiang.i5xforyou.service.UserService;
import com.wuuxiang.i5xforyou.service.ValidateSMSService;
import com.wuuxiang.utils.validator.ValidatorUtil;

@Service("userService")
@Transactional
public class UserServiceImpl extends BaseServiceImpl implements UserService{

	@Autowired
	private ValidateSMSService validateSMSService;
	
	@Autowired
	private MemberCardsService memberCardService;
	
    /**
     * Description: 获取商户短信验证码
     * @author: Mobile Web Group-daizhen
     * @date: 2018年3月27日 下午5:01:21
     * 
     * @see com.wuuxiang.i5xforyou.service.UserService#getSmsVerifyCode(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public ResultMsg getSmsVerifyCode(String mobile, String leftTime, String mpId) {
        // status 0:正常  1:异常错误  2:发送随机验证码、万能验证码。
    	ResultMsg resultMsg = new ResultMsg();
        if (StringUtils.isBlank(mobile) || !ValidatorUtil.isMobile(mobile)) {
        	resultMsg.setState(Results.ERROR);
        	resultMsg.setException(new Exception("请输入正确手机号"));
            return resultMsg;
        }

        // 调用发短信验证码接口
        resultMsg = validateSMSService.getVerifyCode(mobile, leftTime, mpId);
    	if (Results.ERROR.equals(resultMsg.getState())){
    		resultMsg.setState(Results.ERROR);
        	resultMsg.setException(new Exception("调用短信接口发生异常"));
    	}
       	// 正常验证码之正常--短信验证码
		return resultMsg;
    }
    
    /**
     * Description: 验证短信验证码并注册会员申请卡
     * @author: Mobile Web Group-daizhen
     * @date: 2018年3月27日 下午5:01:34
     * 
     * @see com.wuuxiang.i5xforyou.service.UserService#signUserDiner(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public ResultMsg signUserDiner(String nickName, String sex, String verifyCode, String mpId, String openId, String phoneNo, String marketActivityId) {
    	ResultMsg resultMsg = validateSMSService.checkPhoneSmsVerifyCode(phoneNo, verifyCode);
    	if (Results.ERROR.equals(resultMsg.getState())){
        	return resultMsg;
        }
    	ResultMsg getCardsResult = memberCardService.joinMemberCards(nickName, sex, verifyCode, mpId, openId, phoneNo,marketActivityId);
        if (Results.ERROR.equals(getCardsResult.getState())){
        	return getCardsResult;
        }
        resultMsg.setState(Results.SUCCESS);
        resultMsg.setMsgEntity(getCardsResult.getMsgEntity());
		return resultMsg;
    }
    
}
