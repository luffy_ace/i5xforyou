package com.wuuxiang.i5xforyou.service;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.service.BaseService;

/**
 *
 * ClassName: UserService <br/>
 * Function: 用户相关service. <br/>
 * date: 2015年1月15日 下午2:40:36 <br/>
 *
 * @author 李国煜
 * @version
 * @since JDK 1.7
 */
public interface UserService extends BaseService {

    /**
     * getSmsVerifyCode(获取商户短信验证码)
     * @author Mobile Web Group-daizhen
     * @date 2018年3月27日 下午4:28:00
     *
     * @param mobile
     * @param leftTime
     * @param mpId
     * @return
     * @return ResultMsg
     */
    public ResultMsg getSmsVerifyCode(String mobile, String leftTime, String mpId);
  
    /**
     * signUserDiner(注册会员)
     * @author Mobile Web Group-daizhen
     * @date 2018年3月27日 下午4:28:17
     *
     * @param nickName
     * @param sex
     * @param verifyCode
     * @param mpId
     * @param openId
     * @param phoneNo
     * @param marketActivityId
     * @return
     * @return ResultMsg
     */
    public ResultMsg signUserDiner(String nickName, String sex, String verifyCode, String mpId, String openId, String phoneNo, String marketActivityId);
}
