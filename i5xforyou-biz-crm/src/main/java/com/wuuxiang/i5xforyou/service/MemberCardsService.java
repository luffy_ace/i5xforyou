package com.wuuxiang.i5xforyou.service;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.service.BaseService;
import com.wuuxiang.i5xforyou.vo.CrmCard;

public interface MemberCardsService extends BaseService {

	public ResultMsg joinMemberCards(String nickName, String sex, String verifyCode, String mpId,
			String openId, String phoneNo, String marketActivityId);
	
	public CrmCard getPrimaryCard(String mpId, String openId);

}
