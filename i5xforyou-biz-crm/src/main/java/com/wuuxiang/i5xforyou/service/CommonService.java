package com.wuuxiang.i5xforyou.service;

import java.util.List;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.vo.CrmVersion;
import com.wuuxiang.i5xforyou.vo.ShopInfo;

/** 
 * @ClassName: CommonService <br/>
 * crm共同接口service <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午5:21:35
 *
 */
public interface CommonService {
	
	/**
	 * getCrmVersion <br/>
	 * 获取crm版本 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月14日 下午5:31:21
	 *
	 * @param mpId
	 * @return CrmVersion
	 */
	public CrmVersion getCrmVersion(String mpId);
	
	/**
	 * getCrm7Code <br/>
	 * 获取crm7的流水号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月14日 下午5:35:16
	 *
	 * @param mpId
	 * @param openId
	 * @return ResultMsg
	 */
	public ResultMsg getCrm7Code(String mpId, String openId);
	
	/**
	 * getShops <br/>
	 * 获取门店信息的列表 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午4:23:45
	 *
	 * @param mpId
	 * @return List<ShopInfo>
	 */
	public List<ShopInfo> getShops(String mpId);
	
	/**
	 * getGcIdByMpId <br/>
	 * 根据MPID获取GCID <br/>
	 *
	 * @author Mobile Web Group-代震
	 * @date 2018年3月19日 下午4:23:45
	 *
	 * @param mpId
	 * @return String
	 */
	public String getGcIdByMpId(String mpId);
	
	
}
