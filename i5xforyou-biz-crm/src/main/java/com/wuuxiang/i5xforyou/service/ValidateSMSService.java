package com.wuuxiang.i5xforyou.service;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.service.BaseService;

public interface ValidateSMSService extends BaseService {

	/**
	 * getVerifyCode(获取短信验证码)
	 * @author Mobile Web Group-daizhen
	 * @date 2018年3月27日 下午4:29:29
	 *
	 * @param phoneNo
	 * @param leftTime
	 * @param gcId
	 * @return
	 * @return ResultMsg
	 */
	public ResultMsg getVerifyCode(String phoneNo, String leftTime, String gcId);
	
	/**
	 * checkPhoneSmsVerifyCode(验证短信验证码)
	 * @author Mobile Web Group-daizhen
	 * @date 2018年3月27日 下午4:29:45
	 *
	 * @param phoneNo
	 * @param verifyCode
	 * @return
	 * @return ResultMsg
	 */
	public ResultMsg checkPhoneSmsVerifyCode(String phoneNo, String verifyCode);
	
}
