package com.wuuxiang.i5xforyou.vo;

import java.util.ArrayList;
import java.util.List;

public class CrmCards extends CrmBase {
	// 卡列表
	private List<CrmCard> crmCardInfo = new ArrayList<CrmCard>(0);

	public List<CrmCard> getCrmCardInfo() {
		return crmCardInfo;
	}

	// @JSONField(name = "crmCardInfo")
	public void setCrmCardInfo(List<CrmCard> crmCardInfo) {
		this.crmCardInfo = crmCardInfo;
	}

	@Override
	public String toString() {
		return "CrmCards [crmCardInfo=" + crmCardInfo + "]";
	}

}
