package com.wuuxiang.i5xforyou.vo;

public class CrmBase {

	/**
	 * 错误号
	 */
	private Integer v_error_code;

	/**
	 * 错误消息
	 */
	private String v_error_msg;

	public Integer getV_error_code() {
		return v_error_code;
	}

	public void setV_error_code(Integer v_error_code) {
		this.v_error_code = v_error_code;
	}

	public String getV_error_msg() {
		return v_error_msg;
	}

	public void setV_error_msg(String v_error_msg) {
		this.v_error_msg = v_error_msg;
	}

}
