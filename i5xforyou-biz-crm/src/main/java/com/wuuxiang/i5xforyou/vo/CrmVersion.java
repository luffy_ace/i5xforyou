package com.wuuxiang.i5xforyou.vo;

/**
 *
 * @ClassName: CrmVersion
 * @Description: CRM版本枚举类
 * @author Mobile Web Group-lx
 * @date 2017年1月9日 上午10:19:10
 *
 */
public enum CrmVersion {
	CRM6 , CRM7, UNKNOWN
}
