package com.wuuxiang.i5xforyou.vo;

import java.util.Date;

public class PhoSmsVerifyCodeVo {
    private String verifycode;
    private Date createdate;
    private String switch_status;
    public String getVerifycode() {
        return verifycode;
    }
    public void setVerifycode(String verifycode) {
        this.verifycode = verifycode;
    }
    public Date getCreatedate() {
        return createdate;
    }
    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
    
    public String getSwitch_status() {
        return switch_status;
    }
    public void setSwitch_status(String switch_status) {
        this.switch_status = switch_status;
    }
    @Override
    public String toString() {
        return "PhoSmsVerifyCodeVo [verifycode=" + verifycode + ", createdate=" + createdate
                + ", switch_status=" + switch_status + "]";
    }
    
    
}
