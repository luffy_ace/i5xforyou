package com.wuuxiang.i5xforyou.vo;

import org.apache.commons.lang.StringUtils;




public class CrmCard {

	private String isWxAddMoney;
	private String sex;//1:男，2：女
	private String card_kind;
	// 卡积分
	private String cardScore;
	private String shopname;
	private String subshopname;
	private Integer consumeCnt;
	// 卡号
	private String cardNo;
	// 卡余额
	private String balanceMoney;
	private String validEnd;
	private String kindName;
	private String validBegin;
	private String balanceTimes;
	private String mobile;
	private String email;
	private String name;
	// 券数量
	private String ticketCount;
	private String birthday;
	//2015.07.30添加oldBit阴阳历标志位（作者：崔崧）
	private String oldBit;   //生日类型 0阳历，1阴历
	private String remarks;
	
	private String fontColor;
	private String cardStyleImage;
	private String shopname_X;
	private String shopname_Y;
	private String cardNo_X;
	private String cardNo_Y;
	private String cardType_X;
	private String cardType_Y;
	private String shopColor;
	private String cardTypeColor;
	private String mobileColor;
	private String mobile_X;
	private String mobile_Y;
	private String hideMobile;           //中间4位为****格式的手机号，用于保密手机号
	//控制门店、卡号等显示与否开关
	private Integer cardNoShowFlg;    //卡号控制 1：显示；0：不显示
	private Integer shopNameShowFlg;    //门店名称控制 1：显示；0：不显示
	private Integer cardTypeShowFlg;    //卡类型控制 1：显示；0：不显示
	private Integer mobileShowFlg;   //用户手机号控制 1：显示；0：不显示
	
	private String allChargeMoney;
	private String allChargePerMoney;

	private Integer consumeTimes;
	
	//民族
	private String nation;
	//喜好
	private String interest;
	//客人要求
	private String customer_request;
	//会员卡有效期文字说明
	private String cardValidateTag;
	
    public String getMobileColor() {
        return mobileColor;
    }

    public void setMobileColor(String mobileColor) {
        this.mobileColor = mobileColor;
    }

    public String getMobile_X() {
        return mobile_X;
    }

    public void setMobile_X(String mobile_X) {
        this.mobile_X = mobile_X;
    }

    public String getMobile_Y() {
        return mobile_Y;
    }

    public void setMobile_Y(String mobile_Y) {
        this.mobile_Y = mobile_Y;
    }

    public String getHideMobile() {
        return hideMobile;
    }

    public void setHideMobile(String hideMobile) {
        this.hideMobile = hideMobile;
    }

    public Integer getCardNoShowFlg() {
        return cardNoShowFlg;
    }

    public void setCardNoShowFlg(Integer cardNoShowFlg) {
        this.cardNoShowFlg = cardNoShowFlg;
    }

    public Integer getShopNameShowFlg() {
        return shopNameShowFlg;
    }

    public void setShopNameShowFlg(Integer shopNameShowFlg) {
        this.shopNameShowFlg = shopNameShowFlg;
    }

    public Integer getCardTypeShowFlg() {
        return cardTypeShowFlg;
    }

    public void setCardTypeShowFlg(Integer cardTypeShowFlg) {
        this.cardTypeShowFlg = cardTypeShowFlg;
    }

    public Integer getMobileShowFlg() {
        return mobileShowFlg;
    }

    public void setMobileShowFlg(Integer mobileShowFlg) {
        this.mobileShowFlg = mobileShowFlg;
    }

    public String getOldBit() {
		return oldBit;
	}

	public void setOldBit(String oldBit) {
		this.oldBit = oldBit;
	}

	public String getAllChargeMoney() {
        return allChargeMoney;
    }

    public void setAllChargeMoney(String allChargeMoney) {
        this.allChargeMoney = allChargeMoney;
    }

    public String getAllChargePerMoney() {
        return allChargePerMoney;
    }

    public void setAllChargePerMoney(String allChargePerMoney) {
        this.allChargePerMoney = allChargePerMoney;
    }

    public String getShopColor() {
        return shopColor;
    }

    public void setShopColor(String shopColor) {
        this.shopColor = shopColor;
    }

    public String getCardTypeColor() {
        return cardTypeColor;
    }

    public void setCardTypeColor(String cardTypeColor) {
        this.cardTypeColor = cardTypeColor;
    }

    public String getSubshopname() {
		return subshopname;
	}

	public void setSubshopname(String subshopname) {
		this.subshopname = subshopname;
	}

	public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(String ticketCount) {
        this.ticketCount = ticketCount;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getCardStyleImage() {
        return cardStyleImage;
    }

    public void setCardStyleImage(String cardStyleImage) {
        this.cardStyleImage = cardStyleImage;
    }

    public String getShopname_X() {
        return shopname_X;
    }

    public void setShopname_X(String shopname_X) {
        this.shopname_X = shopname_X;
    }

    public String getShopname_Y() {
        return shopname_Y;
    }

    public void setShopname_Y(String shopname_Y) {
        this.shopname_Y = shopname_Y;
    }

    public String getCardNo_X() {
        return cardNo_X;
    }

    public void setCardNo_X(String cardNo_X) {
        this.cardNo_X = cardNo_X;
    }

    public String getCardNo_Y() {
        return cardNo_Y;
    }

    public void setCardNo_Y(String cardNo_Y) {
        this.cardNo_Y = cardNo_Y;
    }

    public String getCardType_X() {
        return cardType_X;
    }

    public void setCardType_X(String cardType_X) {
        this.cardType_X = cardType_X;
    }

    public String getCardType_Y() {
        return cardType_Y;
    }

    public void setCardType_Y(String cardType_Y) {
        this.cardType_Y = cardType_Y;
    }

    public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getConsumeCnt() {
		return consumeCnt;
	}

	public String getIsWxAddMoney() {
		return isWxAddMoney;
	}

	public void setIsWxAddMoney(String isWxAddMoney) {
		this.isWxAddMoney = isWxAddMoney;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCard_kind() {
		return card_kind;
	}

	public void setCard_kind(String card_kind) {
		this.card_kind = card_kind;
	}

	public String getCardScore() {
		return cardScore;
	}

	public void setCardScore(String cardScore) {
		this.cardScore = cardScore;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public void setConsumeCnt(Integer consumeCnt) {
		this.consumeCnt = consumeCnt;
	}

	public String getCardNo() {
		return cardNo;
	}


	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	//@JSONField(name = "cardPrice")
	public String getBalanceMoney() {
		return balanceMoney;
	}
	
	public void setBalanceMoney(String balanceMoney) {
		this.balanceMoney = balanceMoney;
	}

	public String getValidEnd() {
		return validEnd;
	}

	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}

	public String getKindName() {
		return kindName;
	}

	public void setKindName(String kindName) {
		this.kindName = kindName;
	}

	public String getValidBegin() {
		return validBegin;
	}

	public void setValidBegin(String validBegin) {
		this.validBegin = validBegin;
	}

	public String getBalanceTimes() {
		return balanceTimes;
	}

	public void setBalanceTimes(String balanceTimes) {
		this.balanceTimes = balanceTimes;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getConsumeTimes() {
		return consumeTimes;
	}

	public void setConsumeTimes(Integer consumeTimes) {
		this.consumeTimes = consumeTimes;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getCustomer_request() {
		return customer_request;
	}

	public void setCustomer_request(String customer_request) {
		this.customer_request = customer_request;
	}

	public String getCardValidateTag() {
		if (StringUtils.isBlank(cardValidateTag)) {
			cardValidateTag = StringUtils.substring(validBegin, 0, 10) + " 至 " + StringUtils.substring(validEnd, 0, 10);
		}
		return cardValidateTag;
	}

	public void setCardValidateTag(String cardValidateTag) {
		this.cardValidateTag = cardValidateTag;
	}

	@Override
	public String toString() {
		return "CrmCard [isWxAddMoney=" + isWxAddMoney + ", sex=" + sex
				+ ", card_kind=" + card_kind + ", cardScore=" + cardScore
				+ ", shopname=" + shopname + ", consumeCnt=" + consumeCnt
				+ ", cardNo=" + cardNo + ", balanceMoney=" + balanceMoney
				+ ", validEnd=" + validEnd + ", kindName=" + kindName
				+ ", validBegin=" + validBegin + ", balanceTimes="
				+ balanceTimes + ", mobile=" + mobile + ", fontColor=" + fontColor
				+ ", cardStyleImage=" + cardStyleImage + ", shopname_X=" + shopname_X 
				+ ", shopname_Y=" + shopname_Y +", cardNo_X=" + cardNo_X 
				+ ", cardNo_Y=" + cardNo_Y + ", cardType_X=" + cardType_X 
				+ ", cardType_Y=" + cardType_Y +", ticketCount=" + ticketCount 
				+ ", remarks=" + remarks +", oldBit="+oldBit+"]";
	}

}
