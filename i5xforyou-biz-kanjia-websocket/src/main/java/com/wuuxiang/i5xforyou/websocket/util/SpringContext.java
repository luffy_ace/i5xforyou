package com.wuuxiang.i5xforyou.websocket.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: SpringContext <br/>
 * SpringContext <br/>
 * @author Mobile Web Group-lff
 * @date 2018年2月11日 上午10:52:57
 *
 */
@Component
public class SpringContext implements ApplicationContextAware {
	private static Logger log = LoggerFactory.getLogger(SpringContext.class);
	private static ApplicationContext applicationContext;
	
	/**
	 * Description: 获取bean的管理器
	 * @author: Mobile Web Group-lff
	 * @date: 2018年2月11日 上午10:52:58
	 * 
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.info("setApplicationContext，applicationContext=" + applicationContext);
		SpringContext.applicationContext = applicationContext;
	}

	//获取applicationContext
	private static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	//通过name获取 Bean.
	public static Object getBean(String name){
		return getApplicationContext().getBean(name);
	}

	//通过class获取Bean.
	public static <T> T getBean(Class<T> clazz){
		return getApplicationContext().getBean(clazz);
	}

	//通过name,以及Clazz返回指定的Bean
	public static <T> T getBean(String name,Class<T> clazz){
		return getApplicationContext().getBean(name, clazz);
	}
	
}
