package com.wuuxiang.i5xforyou.websocket.rabbitmq.kanjia;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: RabbitMqKanJiaMsgReceiver <br/>
 * 砍价活动的mq消息处理 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年2月11日 下午2:17:34
 *
 */
@Component
@RabbitListener(bindings = @QueueBinding(
				value = @Queue(value = "${websocket.rabbitmq.queue.kanjia.name}", autoDelete = "true"),
				exchange = @Exchange(value = "${websocket.rabbitmq.exchange.topic}", type = ExchangeTypes.TOPIC, durable="true"),
				key = "${websocket.rabbitmq.queue.kanjia.routing-key}")
				)
public class RabbitMqKanJiaMsgReceiver {
	@Autowired
	private KanjiaMsgHandler kanjiaMsgHandler;
	
	
	/**
	 * processMsg <br/>
	 * 处理mq消息，消息格式： <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年2月11日 下午3:11:04
	 *
	 * @param msg
	 * @return void
	 */
	@RabbitHandler
	public void processMsg(String msg) {
		kanjiaMsgHandler.handler(msg);
	}

}
