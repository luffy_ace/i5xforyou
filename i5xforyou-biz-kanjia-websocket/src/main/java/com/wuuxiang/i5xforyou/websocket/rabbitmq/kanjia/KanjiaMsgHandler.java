package com.wuuxiang.i5xforyou.websocket.rabbitmq.kanjia;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wuuxiang.i5xforyou.websocket.rabbitmq.WebSocketRabbitMqBridge;

/** 
 * @ClassName: KanjiaMsgHandler
 * @Description: 砍价websocket消息处理
 * @author Mobile Web Group-lff
 * @date 2018年4月10日 上午8:57:46
 *
 */
@Component
public class KanjiaMsgHandler {
	//mq消息中的address key，代表websocket的地址
	public static final String ADDRESS_KEY_MQ_MSG = "_address";
	private static Logger log = LoggerFactory.getLogger(KanjiaMsgHandler.class);
	//WebSocket和RabbitMq的消息互通bridge
	@Autowired
	private WebSocketRabbitMqBridge webSocketRabbitMqBridge;
	
	@Async("taskExecutor")
	public void handler(String msg) {
		if (StringUtils.isBlank(msg)) {
			//空msg
			return;
		}
		//msg格式：{_address:'kanjia.1234567', type:'barrage', body:{nickname:,photoImg:,placeholder:}}
		try {
			JSONObject msgJson = JSON.parseObject(msg);
			String address = msgJson.getString(ADDRESS_KEY_MQ_MSG);
			webSocketRabbitMqBridge.publishWebSocketMsg(address, msg);
			log.info("server send msg:address=" + address);
		} catch (Exception e) {
			log.error("消息处理异常：" + e.toString() + ",msg=" + msg);
		}
	}
	
}
