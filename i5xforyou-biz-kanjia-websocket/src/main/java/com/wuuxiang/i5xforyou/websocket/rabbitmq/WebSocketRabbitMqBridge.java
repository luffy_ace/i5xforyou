package com.wuuxiang.i5xforyou.websocket.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.wuuxiang.i5xforyou.websocket.WebSocket;

/** 
 * @ClassName: WebSocketRabbitMqBridge <br/>
 * WebSocket和RabbitMq的消息互通bridge <br/>
 * @author Mobile Web Group-lff
 * @date 2018年2月12日 上午8:56:00
 *
 */
@Component
public class WebSocketRabbitMqBridge {

	//websocket和rabbitmq传递消息的exchange
	@Value("${websocket.rabbitmq.exchange.topic}")
	private String websocket_rabbitmq_exchange_topic;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	
	/**
	 * sendRabbitMqMsg <br/>
	 * 将websocket消息转发到rabbitmq上，进行服务间传播 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年2月12日 上午9:02:19
	 *
	 * @param exchange
	 * @param routingKey
	 * @param object
	 * @return void
	 */
	public void sendRabbitMqMsg(String routingKey, final Object object) {
		rabbitTemplate.convertAndSend(websocket_rabbitmq_exchange_topic, routingKey, object);
	}
	
	
	/**
	 * publish <br/>
	 * 广播websocket消息 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年2月11日 下午3:09:21
	 *
	 * @param address
	 * @param msg
	 * @return void
	 */
	public void publishWebSocketMsg(String address, String msg) {
		WebSocket.publish(address, msg);
	}
	
	
}
