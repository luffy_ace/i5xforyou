# i5xforyou -- 爱吾享为你专属定制的服务

##https://gitee.com/ankeway/i5xforyou

> i5xforyou是吾享移动WEB组倾情奉献的爱吾享系列产品中一款新的微服务项目
> 其开发基于SpringBoot2.0Release版本和SpringCloudFinchley.M8版本

### 注册中心 (Spring Cloud Eureka)
> i5xforyou-service-registry

### 配置中心 (Spring Cloud Config)
> i5xforyou-service-config
> 为了统一运维，配置中心Server在提供自身配置服务基础上，经过二次开发对接PoleStar配置中心，调整poleStar的参数后，通过Spring Cloud Bus端点刷新实现所有Config客户端的更新
> 内置使用Spring Cloud Bus，以rabbitmq的方式实现分布式刷新


### 网关 (Spring Cloud Gateway)
> i5xforyou-service-gateway

### 通用模块
> i5xforyou-service-common

### 通用工具包
> i5xforyou-service-utils

### 测试客户端
> i5xforyou-support-service-testclient