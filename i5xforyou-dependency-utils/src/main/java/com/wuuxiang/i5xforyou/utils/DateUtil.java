package com.wuuxiang.i5xforyou.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author 微信攻略组
 *
 */
public class DateUtil {
	// ---当前日期的年，月，日，时，分，秒
	public static Calendar now = Calendar.getInstance();

	// 日期格式，年份，例如：2004，2008
	public static final String YYYY = "yyyy";

	// 日期格式，年份和月份，例如：200707，200808
	public static final String YYYYMM = "yyyyMM";

	// 日期格式，年份和月份，例如：2007-07，2008-08
	public static final String YYYY_MM = "yyyy-MM";

	// 日期格式，年月日，例如：20050630，20080808
	public static final String YYYYMMDD = "yyyyMMdd";

	// 日期格式，年月日，用横杠分开，例如：2006-12-25，2008-08-08
	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	// 日期格式，年月日时分秒，例如：20001230120000，20080808200808
	public static final String YYYYMMDDHHMISS = "yyyyMMddHHmmss";

	// 日期格式，年月日时分秒，年月日用横杠分开，时分秒用冒号分开，
	// 例如：2005-05-10 23：20：00，2008-08-08 20:08:08
	public static final String YYYY_MM_DD_HH_MI_SS = "yyyy-MM-dd HH:mm:ss";

	// 日期格式，年月日时分，例如：16:40
	public static final String HHMI = "HH:mm";

	// 日期格式，年月日时分秒，例如：16:40:49
	public static final String HHMISS = "HH:mm:ss";

	// 日期格式，年月日时分秒，例如：16:40:49
	public static final String YYYY_MM_DD_HH_MI = "yyyy-MM-dd HH:mm";


	/**
	 * parse <br/>
	 * datePattern格式的dateStr字符串转换为date对象 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午4:11:43
	 *
	 * @param datePattern
	 * @param dateStr
	 * @return Date
	 */
	public static Date parse(String datePattern, String dateStr) {
		if (StringUtils.isBlank(dateStr)) {
			return null;
		}
		try {
			return new SimpleDateFormat(datePattern).parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * format <br/>
	 * date对象转换为datePattern格式的字符串 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午4:13:39
	 *
	 * @param datePattern
	 * @param date
	 * @return String
	 */
	public static String format(String datePattern, Date date) {
		if (date == null) {
			return null;
		}
		return new SimpleDateFormat(datePattern).format(date);
	}
	
	/**
	 * format <br/>
	 * dateStr字符串 转换为datePattern格式的date字符串 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午4:13:39
	 *
	 * @param datePattern
	 * @param dateStr
	 * @return String
	 */
	public static String format(String datePattern, String dateStr) {
		if (StringUtils.isBlank(dateStr) || StringUtils.isBlank(datePattern)) {
			return dateStr;
		}
		if (dateStr.length() <= datePattern.length()) {
			//不能把短的时间格式转换为长的时间格式
			return dateStr;
		}
		return dateStr.substring(0, datePattern.length());
	}
	
	/**
	 * 日期时间转换为字符串
	 *
	 * @author dingyongli
	 * @param Date
	 *            date：需要转换的日期
	 * @param String
	 *            format：转换格式
	 * @return String
	 * @throws
	 */
	public final static String dt2s(Date date, String format) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat(format);
		return dateTimeFormat.format(date);
	}

}