/**  
 * @Title: MoneyFormatUtil.java
 * @Package com.wuuxiang.i5xwxplus.util
 * @Description: 价格format
 * Copyright: Copyright (c) 2016 Wuuxiang All Rights Reserved.
 * Company: 吾享(天津)网络科技有限公司
 * 
 * @author Mobile Web Group-lff
 * @date 2016年12月5日 下午4:33:20
 * @version V1.0
 */

package com.wuuxiang.i5xforyou.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;

/** 
 * @ClassName: MoneyFormatUtil
 * @Description: 价格format,小数点后两位
 * @author Mobile Web Group-lff
 * @date 2016年12月5日 下午4:33:20
 *
 */

public class MoneyFormatUtil {
	//金额格式化
	private static final DecimalFormat moneyFormat = new DecimalFormat("#0.##");
	
	/**
	 * format
	 * @Description: 价格format,小数点后两位
	 * @author Mobile Web Group-lff
	 * @date 2016年12月5日 下午4:38:04
	 *
	 * @param price
	 * @param blankValue:price为blank时的返回值
	 * @return String
	 */
	public static String format(String price, String blankValue) {
		if (StringUtils.isNotBlank(price)) {
			return format(new BigDecimal(price), blankValue);
		}
		return blankValue;
	}
	
	/**
	 * format
	 * @Description: 价格format,小数点后两位
	 * @author Mobile Web Group-lff
	 * @date 2016年12月5日 下午4:38:19
	 *
	 * @param price
	 * @param nullValue:price为null时的返回值
	 * @return String
	 */
	public static String format(BigDecimal price, String nullValue) {
		if (price != null) {
			return moneyFormat.format(price);
		}
		return nullValue;
	}
}
