package com.wuuxiang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer //启动一个服务注册中心提供给其他应用进行对话
public class Application {

	public static void main(String[] args) {
		//new SpringApplicationBuilder(ServiceRegistryApplication.class).web(true).run(args);
		SpringApplication.run(Application.class, args);
	}

}
