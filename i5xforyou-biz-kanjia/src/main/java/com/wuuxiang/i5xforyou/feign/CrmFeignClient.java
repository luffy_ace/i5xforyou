package com.wuuxiang.i5xforyou.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wuuxiang.i5xforyou.common.domain.JsonPackage;

/** 
 * @ClassName: CrmFeignClient <br/>
 * 调用crm服务的接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午2:44:14
 *
 */
@FeignClient(name = "i5xforyou-biz-crm", fallback=CrmFeignClientHystrix.class)
public interface CrmFeignClient {
	
	/**
	 * getCrmVersion <br/>
	 * 获取crm的版本号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:15:19
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/crm/common/crmversion", method = RequestMethod.GET)
	public JsonPackage getCrmVersion(@RequestParam("mpId") String mpId);
	
	/**
	 * getCrm7Code <br/>
	 * 获取crm7的流水号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:15:37
	 *
	 * @param mpId
	 * @param openId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/crm/common/crm7code", method = RequestMethod.GET)
	public JsonPackage getCrm7Code(@RequestParam("mpId") String mpId, @RequestParam("openId") String openId);
	
	/**
	 * getShops <br/>
	 * 获取门店信息的列表 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午4:23:04
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/crm/common/shops", method = RequestMethod.GET)
	public JsonPackage getShops(@RequestParam("mpId") String mpId);
	
	
	
	
	
}
