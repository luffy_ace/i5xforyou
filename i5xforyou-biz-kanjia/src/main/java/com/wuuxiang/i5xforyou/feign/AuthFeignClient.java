package com.wuuxiang.i5xforyou.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wuuxiang.i5xforyou.common.domain.JsonPackage;

/** 
 * @ClassName: AuthFeignClient <br/>
 * 调用auth服务的接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午2:44:14
 *
 */
@FeignClient(name = "i5xforyou-biz-auth", fallback=AuthFeignClientHystrix.class)
public interface AuthFeignClient {
	
	/**
	 * getMiniProgramInfo <br/>
	 * 获取小程序的基本信息 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月22日 上午9:14:44
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/auth/miniprogram/info", method = RequestMethod.GET)
	public JsonPackage getMiniProgramInfo(@RequestParam("mpId") String mpId);
	
	
	
}
