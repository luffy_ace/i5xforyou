package com.wuuxiang.i5xforyou.feign;

import org.springframework.stereotype.Component;

import com.wuuxiang.i5xforyou.common.BaseClass;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;

/** 
 * @ClassName: CrmFeignClientHystrix <br/>
 * 调用crm服务接口的断路由 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午2:45:54
 *
 */
@Component
public class CrmFeignClientHystrix extends BaseClass implements CrmFeignClient {

	/**
	 * Description: 获取crm的版本号
	 * @author: Mobile Web Group-lff
	 * @date: 2018年3月15日 下午2:51:05
	 * 
	 * @see com.wuuxiang.i5xforyou.feign.CrmFeignClient#getCrmVersion(java.lang.String)
	 */
	@Override
	public JsonPackage getCrmVersion(String mpId) {
		log.error("调用i5xforyou-biz-crm的/common/crmversion接口触发了断路由，入参mpId=" + mpId);
		return JsonPackage.getHystrixJsonPackage();
	}

	/**
	 * Description: 获取crm7的流水号
	 * @author: Mobile Web Group-lff
	 * @date: 2018年3月21日 下午3:35:26
	 * 
	 * @see com.wuuxiang.i5xforyou.feign.CrmFeignClient#getCrm7Code(java.lang.String, java.lang.String)
	 */
	@Override
	public JsonPackage getCrm7Code(String mpId, String openId) {
		log.error("调用i5xforyou-biz-crm的/common/crm7code接口触发了断路由，入参mpId=" + mpId + ",openId=" + openId);
		return JsonPackage.getHystrixJsonPackage();
	}

	/**
	 * Description: 获取门店信息的列表
	 * @author: Mobile Web Group-lff
	 * @date: 2018年3月19日 下午4:38:18
	 * 
	 * @see com.wuuxiang.i5xforyou.feign.CrmFeignClient#getShops(java.lang.String)
	 */
	@Override
	public JsonPackage getShops(String mpId) {
		log.error("调用i5xforyou-biz-crm的/common/shops接口触发了断路由，入参mpId=" + mpId);
		return JsonPackage.getHystrixJsonPackage();
	}



}
