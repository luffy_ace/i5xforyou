package com.wuuxiang.i5xforyou.feign.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.feign.AuthFeignClient;
import com.wuuxiang.i5xforyou.feign.CrmFeignClient;
import com.wuuxiang.i5xforyou.vo.coupon.ShopInfo;

/** 
 * @ClassName: FeignClientService <br/>
 * feign通用实现类,可以封装降级等处理 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月16日 下午5:56:50
 *
 */
@Component
public class FeignClientService  {
	@Autowired
	private CrmFeignClient crmFeignClient;
	
	@Autowired
	private AuthFeignClient authFeignClient;
	
	/**
	 * getFeignCrmVersion <br/>
	 * 获取crm版本 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午5:58:48
	 *
	 * @param mpId
	 * @return String
	 */
	public JsonPackage getCrmVersion(String mpId) {
		//获取crm版本
		return crmFeignClient.getCrmVersion(mpId);
	}
	
	/**
	 * getFeignCrm7Code <br/>
	 * 获取crm7的流水号 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:15:37
	 *
	 * @param mpId
	 * @param openId
	 * @return String
	 */
	public JsonPackage getCrm7Code(String mpId, String openId) {
		//获取crm7的流水号
		return crmFeignClient.getCrm7Code(mpId, openId);
	}
	
	/**
	 * getFeignShops <br/>
	 * 获取门店信息列表, 非主要业务，可以降级 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午4:47:05
	 *
	 * @param mpId
	 * @return Map<Integer, ShopInfo>
	 */
	public Map<Integer, ShopInfo> getShops(String mpId) {
		Map<Integer, ShopInfo> shopInfoMap = new HashMap<Integer, ShopInfo>();
		JsonPackage jsonPackage = crmFeignClient.getShops(mpId);
		//获取门店信息失败，降级
		if (jsonPackage.getStatus() != 0) {
			return shopInfoMap;
		}
		JSONArray shopInfoArray = (JSONArray) JSON.toJSON(jsonPackage.getResult());
		
		if (shopInfoArray == null) {
			//没有数据
			return shopInfoMap;
		}
		//jsonArray转Map<Integer, ShopInfo>
		for (int i = 0; i < shopInfoArray.size(); i++) {
			JSONObject shopInfoJson = shopInfoArray.getJSONObject(i);
			ShopInfo shopInfo = new ShopInfo();
			shopInfo.setMcid(shopInfoJson.getInteger("mcid"));
			shopInfo.setAddress(shopInfoJson.getString("address"));
			shopInfo.setName(shopInfoJson.getString("name"));
			shopInfo.setOrdertel(shopInfoJson.getString("ordertel"));
			
			shopInfoMap.put(shopInfo.getMcid(), shopInfo);
		}
		return shopInfoMap;
	}
	
	/**
	 * getFeignMiniProgramInfo <br/>
	 * 获取小程序的基本信息 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月22日 上午9:25:39
	 *
	 * @param mpId
	 * @return WxMiniProgram
	 */
	public JsonPackage getMiniProgramInfo(String mpId) {
		//获取小程序信息
		return authFeignClient.getMiniProgramInfo(mpId);
	}
	
	
	
}
