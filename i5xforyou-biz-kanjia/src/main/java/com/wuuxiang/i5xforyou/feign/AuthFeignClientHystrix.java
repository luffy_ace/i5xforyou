package com.wuuxiang.i5xforyou.feign;

import org.springframework.stereotype.Component;

import com.wuuxiang.i5xforyou.common.BaseClass;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;

/** 
 * @ClassName: AuthFeignClientHystrix <br/>
 * 调用auth服务接口的断路由 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午2:45:54
 *
 */
@Component
public class AuthFeignClientHystrix extends BaseClass implements AuthFeignClient {

	/**
	 * Description: 获取小程序的基本信息
	 * @author: Mobile Web Group-lff
	 * @date: 2018年3月22日 上午9:17:35
	 * 
	 * @see com.wuuxiang.i5xforyou.feign.AuthFeignClient#getMiniProgramInfo(java.lang.String)
	 */
	@Override
	public JsonPackage getMiniProgramInfo(String mpId) {
		log.error("调用i5xforyou-biz-auth的/miniprogram/info接口触发了断路由，入参mpId=" + mpId);
		return JsonPackage.getHystrixJsonPackage();
	}




}
