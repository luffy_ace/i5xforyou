package com.wuuxiang.i5xforyou.vo.kanjia;

import java.math.BigDecimal;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.wuuxiang.i5xforyou.kanjia.consts.KanJiaConsts;

public class TradeResult {
	// 砍掉的金额
	private BigDecimal cutMoney;
	// 砍掉总金额
	private BigDecimal cutTotalMoney;
	// 剩余可砍总金额
	private BigDecimal cutLeftMoney;
	// 帮砍次数
	private int bargainTimes;
	// 发起人头像6
	private String initiatorUserName;
	// 发起人头像7
	//private String initiatorNickname;
	// 发起人姓名
	private String initiatorProfilePhotoImg;
	// 发起人姓名
	//private String initiatorProfilePic;
	// 赠送券列表
	private List<GiftCoupon> coupons;
	
	// 辅助模型字段
	private Integer status;//活动状态(说明见接口说明)
	private String modalImg;//根据活动状态出提示：弹层的背景图
	private String modalText;//根据活动状态出提示：弹层的提示
	

	public void setInitiatorNickname(String initiatorNickname) {
		this.initiatorUserName = initiatorNickname;
	}

	public void setInitiatorProfilePic(String initiatorProfilePic) {
		this.initiatorProfilePhotoImg = initiatorProfilePic;
	}

	public BigDecimal getCutMoney() {
		return cutMoney;
	}

	public void setCutMoney(BigDecimal cutMoney) {
		this.cutMoney = cutMoney;
	}

	public BigDecimal getCutTotalMoney() {
		return cutTotalMoney;
	}

	public void setCutTotalMoney(BigDecimal cutTotalMoney) {
		this.cutTotalMoney = cutTotalMoney;
	}

	public BigDecimal getCutLeftMoney() {
		return cutLeftMoney;
	}

	public void setCutLeftMoney(BigDecimal cutLeftMoney) {
		this.cutLeftMoney = cutLeftMoney;
	}

	public int getBargainTimes() {
		return bargainTimes;
	}

	public void setBargainTimes(int bargainTimes) {
		this.bargainTimes = bargainTimes;
	}

	public String getInitiatorUserName() {
		return initiatorUserName;
	}

	public void setInitiatorUserName(String initiatorUserName) {
		this.initiatorUserName = initiatorUserName;
	}

	public String getInitiatorProfilePhotoImg() {
		return initiatorProfilePhotoImg;
	}

	public void setInitiatorProfilePhotoImg(String initiatorProfilePhotoImg) {
		this.initiatorProfilePhotoImg = initiatorProfilePhotoImg;
	}

	public List<GiftCoupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<GiftCoupon> coupons) {
		this.coupons = coupons;
	}
	
	public String getModalImg() {
		return modalImg;
	}

	/**
	 * createModalInfo <br/>
	 * 根据活动状态设置提示的弹层信息 <br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月20日 上午9:42:41
	 *
	 * @return void
	 */
	public Boolean createModalInfo(Integer status) {
		this.status = status;
		if(status == 1 || status == 200) {
			this.status = 0;
			modalText = "成功";
		}else if (status == 402) {
			modalText = KanJiaConsts.TRADE_STATUS_402_MSG;
		} else if (status == 403) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_NONE_IMG;
			modalText = KanJiaConsts.TRADE_STATUS_403_MSG;
		} else if (status == 404) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_END_IMG;
			modalText = KanJiaConsts.TRADE_STATUS_404_MSG;
		} else if (status == 406) {
			modalText = KanJiaConsts.TRADE_STATUS_406_MSG;
		} else if (status == 408) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_END_IMG;
			modalText = KanJiaConsts.TRADE_STATUS_408_MSG;
		} else if (status == 409) {
			modalText = KanJiaConsts.TRADE_STATUS_409_MSG;
		} else {
			return false;
		}
		return true;
	}
	
	// 获取状态
	public Integer takeStatus() {
		return status;
	}
	// 获取报错信息
	public String takeModalText() {
		return modalText;
	}
	
	
	public static void main(String[] args) {
		System.out.println(JSON.toJSONString(new TradeResult() ));
	}


}
