package com.wuuxiang.i5xforyou.vo.kanjia;

import java.math.BigDecimal;
import java.util.List;

import com.wuuxiang.i5xforyou.kanjia.consts.KanJiaConsts;


/** 
 * @ClassName: Activity <br/>
 * 砍价活动bean <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午4:53:04
 *
 */
public class Activity {
	private Integer bargainType;//砍价方式 1：多少人砍至底价；2：区间随机砍价
	private Integer bargainPeopleNum;//砍价规则多少人砍至底价
	private BigDecimal bargainMinMoney;//砍价规则每人可砍价随机最低区间
	private BigDecimal bargainMaxMoney;//砍价规则每人可砍价随机最大区间
	private Integer barrage;//弹幕开关 0：关闭；1：开启
	private Integer helpReward;//帮砍人奖励是否开启 0：关闭；1：开启
	private Integer bargainTimes;//当前已砍价次数
	private BigDecimal cutMoney;//砍掉多少元
	private Integer bargainRanking;//本人该活动的名次
	private String twoDimensionalCode;//公众号二维码
	private String backgroundImg;//背景图片
	private String shareImg;//分享图标
	private String shareTitle;//分享标题
	private String shareContent;//分享描述
	private String content;//活动描述
	private String startDate;//开始时间
	private String endDate;//结束时间
	private String currentDate;//当前时间
	private String name;//活动名称
	private Integer status;//活动状态(说明见接口说明)
	private String modalImg;//根据活动状态出提示：弹层的背景图
	private String modalText;//根据活动状态出提示：弹层的提示语
	private List<Coupon> coupons;//优惠卷
	
	/**
	 * setModalInfo <br/>
	 * 根据活动状态设置提示的弹层信息 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月20日 上午9:42:41
	 *
	 * @return void
	 */
	public void setStatusModalInfo() {
		if (status == null) {
			return;
		} else if (status == 2) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_END_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_2_MSG;
		} else if (status == 3) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_END_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_3_MSG;
		} else if (status == 4) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_NONE_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_4_MSG;
		} else if (status == 111) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_END_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_111_MSG;
		} else if (status == 411) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_NONE_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_411_MSG;
		} else if (status == 412) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_NONE_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_412_MSG;
		} else if (status == 414) {
			modalImg = KanJiaConsts.ACTIVITY_STATUS_NONE_IMG;
			modalText = KanJiaConsts.ACTIVITY_STATUS_414_MSG;
		}
	}
	
	public Integer getBargainType() {
		return bargainType;
	}
	public void setBargainType(Integer bargainType) {
		this.bargainType = bargainType;
	}
	public Integer getBargainPeopleNum() {
		return bargainPeopleNum;
	}
	public void setBargainPeopleNum(Integer bargainPeopleNum) {
		this.bargainPeopleNum = bargainPeopleNum;
	}
	public BigDecimal getBargainMinMoney() {
		return bargainMinMoney;
	}
	public void setBargainMinMoney(BigDecimal bargainMinMoney) {
		this.bargainMinMoney = bargainMinMoney;
	}
	public BigDecimal getBargainMaxMoney() {
		return bargainMaxMoney;
	}
	public void setBargainMaxMoney(BigDecimal bargainMaxMoney) {
		this.bargainMaxMoney = bargainMaxMoney;
	}
	public Integer getBarrage() {
		return barrage;
	}
	public void setBarrage(Integer barrage) {
		this.barrage = barrage;
	}
	public Integer getHelpReward() {
		return helpReward;
	}
	public void setHelpReward(Integer helpReward) {
		this.helpReward = helpReward;
	}
	public Integer getBargainTimes() {
		return bargainTimes;
	}
	public void setBargainTimes(Integer bargainTimes) {
		this.bargainTimes = bargainTimes;
	}
	public BigDecimal getCutMoney() {
		return cutMoney;
	}
	public void setCutMoney(BigDecimal cutMoney) {
		this.cutMoney = cutMoney;
	}
	public Integer getBargainRanking() {
		return bargainRanking;
	}
	public void setBargainRanking(Integer bargainRanking) {
		this.bargainRanking = bargainRanking;
	}
	public String getTwoDimensionalCode() {
		return twoDimensionalCode;
	}
	public void setTwoDimensionalCode(String twoDimensionalCode) {
		this.twoDimensionalCode = twoDimensionalCode;
	}
	public String getBackgroundImg() {
		return backgroundImg;
	}
	public void setBackgroundImg(String backgroundImg) {
		this.backgroundImg = backgroundImg;
	}
	public String getShareImg() {
		return shareImg;
	}
	public void setShareImg(String shareImg) {
		this.shareImg = shareImg;
	}
	public String getShareTitle() {
		return shareTitle;
	}
	public void setShareTitle(String shareTitle) {
		this.shareTitle = shareTitle;
	}
	public String getShareContent() {
		return shareContent;
	}
	public void setShareContent(String shareContent) {
		this.shareContent = shareContent;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getModalImg() {
		return modalImg;
	}
	public void setModalImg(String modalImg) {
		this.modalImg = modalImg;
	}
	public String getModalText() {
		return modalText;
	}
	public void setModalText(String modalText) {
		this.modalText = modalText;
	}
	public List<Coupon> getCoupons() {
		return coupons;
	}
	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}
	
	
}
