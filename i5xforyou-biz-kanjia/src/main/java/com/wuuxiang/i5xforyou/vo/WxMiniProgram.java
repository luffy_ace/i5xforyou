package com.wuuxiang.i5xforyou.vo;
import java.io.Serializable;
import java.util.Date;
/**
 * @ClassName: WxMiniProgram
 * @Description: 商户授权信息授权
 * @author Mobile Web Group-LiuYandong
 * @date 2018年3月15日 上午10:15:31
 *
 */
public class WxMiniProgram implements Serializable {
	private static final long serialVersionUID = -6197681199219625149L;
	//小程序APPID
	private Integer id;
	//小程序APPID
	private String cappid;
	//小程序原始ID
	private String cmpid;
	//小程序原始ID
	private String cnickname;
	//小程序访问API资源票据
	private String caccesstoken;
	//小程序访问API资源票据更新时间
	private Date dtaccesstoken;
	//第三方平台授权更新TOKEN
	private String cauthrefreshtoken;
	//0未授权，1已授权，2取消授权
	private Integer iauthflg;
	//创建时间
	private Date dtcreate;
	//更新时间
	private Date dtupdate;
	//0：未删除 1：已删除
	private Integer ideleteflg;
	//删除时间
	private Date dtdelete;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCappid() {
		return cappid;
	}
	public void setCappid(String cappid) {
		this.cappid = cappid;
	}
	public String getCmpid() {
		return cmpid;
	}
	public void setCmpid(String cmpid) {
		this.cmpid = cmpid;
	}
	public String getCnickname() {
		return cnickname;
	}
	public void setCnickname(String cnickname) {
		this.cnickname = cnickname;
	}
	public String getCaccesstoken() {
		return caccesstoken;
	}
	public void setCaccesstoken(String caccesstoken) {
		this.caccesstoken = caccesstoken;
	}
	public Date getDtaccesstoken() {
		return dtaccesstoken;
	}
	public void setDtaccesstoken(Date dtaccesstoken) {
		this.dtaccesstoken = dtaccesstoken;
	}
	public String getCauthrefreshtoken() {
		return cauthrefreshtoken;
	}
	public void setCauthrefreshtoken(String cauthrefreshtoken) {
		this.cauthrefreshtoken = cauthrefreshtoken;
	}
	public Integer getIauthflg() {
		return iauthflg;
	}
	public void setIauthflg(Integer iauthflg) {
		this.iauthflg = iauthflg;
	}
	public Date getDtcreate() {
		return dtcreate;
	}
	public void setDtcreate(Date dtcreate) {
		this.dtcreate = dtcreate;
	}
	public Date getDtupdate() {
		return dtupdate;
	}
	public void setDtupdate(Date dtupdate) {
		this.dtupdate = dtupdate;
	}
	public Integer getIdeleteflg() {
		return ideleteflg;
	}
	public void setIdeleteflg(Integer ideleteflg) {
		this.ideleteflg = ideleteflg;
	}
	public Date getDtdelete() {
		return dtdelete;
	}
	public void setDtdelete(Date dtdelete) {
		this.dtdelete = dtdelete;
	}

}
