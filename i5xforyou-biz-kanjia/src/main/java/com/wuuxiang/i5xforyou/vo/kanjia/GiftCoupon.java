package com.wuuxiang.i5xforyou.vo.kanjia;

import java.math.BigDecimal;

public class GiftCoupon {
	// 赠券id
	private String id;
	// 赠券名称
	private String title;
	// 赠券金额
	private BigDecimal value;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	//crm7 code 做id
	public void setCode(String id) {
		this.id = id;
	}
}
