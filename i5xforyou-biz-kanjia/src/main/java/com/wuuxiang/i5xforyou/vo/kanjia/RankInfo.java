package com.wuuxiang.i5xforyou.vo.kanjia;

import java.math.BigDecimal;

/** 
 * @ClassName: RankInfo
 * @Description: 排行信息
 * @author Mobile Web Group-lff
 * @date 2018年4月10日 下午1:00:29
 *
 */
public class RankInfo {
	private String userName;//昵称
	private String profilePhotoImg;//头像
	private BigDecimal cutToMoney;//砍到的金额
	private Integer helperNum;//帮砍人数
	private Integer myRanking;//当前排行
	private BigDecimal cutMoney;//砍价金额
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProfilePhotoImg() {
		return profilePhotoImg;
	}
	public void setProfilePhotoImg(String profilePhotoImg) {
		this.profilePhotoImg = profilePhotoImg;
	}
	public BigDecimal getCutToMoney() {
		return cutToMoney;
	}
	public void setCutToMoney(BigDecimal cutToMoney) {
		this.cutToMoney = cutToMoney;
	}
	public Integer getHelperNum() {
		return helperNum;
	}
	public void setHelperNum(Integer helperNum) {
		this.helperNum = helperNum;
	}
	public Integer getMyRanking() {
		return myRanking;
	}
	public void setMyRanking(Integer myRanking) {
		this.myRanking = myRanking;
	}
	public BigDecimal getCutMoney() {
		return cutMoney;
	}
	public void setCutMoney(BigDecimal cutMoney) {
		this.cutMoney = cutMoney;
	}
	
}
