package com.wuuxiang.i5xforyou.vo.coupon;

/** 
 * @ClassName: ShopInfo <br/>
 * 门店列表的门店信息 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月19日 下午2:28:37
 *
 */
public class ShopInfo {
	private Integer mcid;
	private String address;//门店地址
	private String ordertel;//门店电话号码
	private String name;//门店名称
	
	public Integer getMcid() {
		return mcid;
	}
	public void setMcid(Integer mcid) {
		this.mcid = mcid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOrdertel() {
		return ordertel;
	}
	public void setOrdertel(String ordertel) {
		this.ordertel = ordertel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
