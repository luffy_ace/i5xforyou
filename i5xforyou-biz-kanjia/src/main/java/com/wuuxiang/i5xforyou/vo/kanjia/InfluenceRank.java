package com.wuuxiang.i5xforyou.vo.kanjia;

import java.util.List;

/** 
 * @ClassName: InfluenceRank
 * @Description: 影响力排行榜
 * @author Mobile Web Group-lff
 * @date 2018年4月10日 下午12:59:00
 *
 */
public class InfluenceRank {
	//top5排行
	private List<RankInfo> top5data;
	//我的排行
	private RankInfo myRankingData;
	
	public List<RankInfo> getTop5data() {
		return top5data;
	}
	public void setTop5data(List<RankInfo> top5data) {
		this.top5data = top5data;
	}
	public RankInfo getMyRankingData() {
		return myRankingData;
	}
	public void setMyRankingData(RankInfo myRankingData) {
		this.myRankingData = myRankingData;
	}
	
}
