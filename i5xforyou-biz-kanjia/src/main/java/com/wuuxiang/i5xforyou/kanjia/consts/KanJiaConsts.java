package com.wuuxiang.i5xforyou.kanjia.consts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: KanJiaConsts <br/>
 * 共通变量，对应i5xforyou-biz-kanjia中的变量 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 上午8:39:59
 *
 */
@Component
@RefreshScope
public class KanJiaConsts {
	/*
	 * 重要，对静态static变量通过@value赋值，要通过非静态的set方法才行
	 */
	
	/** 
	 *  砍价初始化变量
	 */
	/* 抢光了的背景图  (B)*/
	public static String ACTIVITY_STATUS_NONE_IMG;
	/* 活动结束的背景图  (A)*/
	public static String ACTIVITY_STATUS_END_IMG;
	/* 2:系统结束的提示语 */
	public static String ACTIVITY_STATUS_2_MSG;
	/* 3:人工结束的提示语 */
	public static String ACTIVITY_STATUS_3_MSG;
	/* 4:不可砍价的提示语 */
	public static String ACTIVITY_STATUS_4_MSG;
	/* 111:券被抢光了的提示语 */
	public static String ACTIVITY_STATUS_111_MSG;
	/* 411:购买者已购买不可砍价的提示语 */
	public static String ACTIVITY_STATUS_411_MSG;
	/* 412:购买者正在支付不可砍价*/
	public static String ACTIVITY_STATUS_412_MSG;
	/* 414:已到底价不可砍价 */
	public static String ACTIVITY_STATUS_414_MSG;
	/** 
	 *  砍价过程中变量
	 */
	/* 402:您已进行过砍价,请勿重复操作!*/
	public static String TRADE_STATUS_402_MSG;
	/* 403:此活动当前不能进行砍价! */
	public static String TRADE_STATUS_403_MSG;
	/* 404:当前已经到了低价,不能继续再进行砍价了! */
	public static String TRADE_STATUS_404_MSG;
	/* 406:当前砍价操作繁忙,请稍后重试! */
	public static String TRADE_STATUS_406_MSG;
	/* 407:当前活动还未开始!
	public static String TRADE_STATUS_407_MSG; */
	/* 408:当前活动已经结束!*/
	public static String TRADE_STATUS_408_MSG;
	/* 409:没有有效的活动信息 */
	public static String TRADE_STATUS_409_MSG;
	
	@Value("${activity.status.none.img:''}")
	public void setACTIVITY_STATUS_NONE_IMG(String aCTIVITY_STATUS_NONE_IMG) {
		ACTIVITY_STATUS_NONE_IMG = aCTIVITY_STATUS_NONE_IMG;
	}
	@Value("${activity.status.end.img:''}")
	public void setACTIVITY_STATUS_END_IMG(String aCTIVITY_STATUS_END_IMG) {
		ACTIVITY_STATUS_END_IMG = aCTIVITY_STATUS_END_IMG;
	}
	@Value("${activity.status.2.msg:''}")
	public void setACTIVITY_STATUS_2_MSG(String aCTIVITY_STATUS_2_MSG) {
		ACTIVITY_STATUS_2_MSG = aCTIVITY_STATUS_2_MSG;
	}
	@Value("${activity.status.3.msg:''}")
	public void setACTIVITY_STATUS_3_MSG(String aCTIVITY_STATUS_3_MSG) {
		ACTIVITY_STATUS_3_MSG = aCTIVITY_STATUS_3_MSG;
	}
	@Value("${activity.status.4.msg:''}")
	public void setACTIVITY_STATUS_4_MSG(String aCTIVITY_STATUS_4_MSG) {
		ACTIVITY_STATUS_4_MSG = aCTIVITY_STATUS_4_MSG;
	}
	@Value("${activity.status.111.msg:''}")
	public void setACTIVITY_STATUS_111_MSG(String aCTIVITY_STATUS_111_MSG) {
		ACTIVITY_STATUS_111_MSG = aCTIVITY_STATUS_111_MSG;
	}
	@Value("${activity.status.411.msg:''}")
	public void setACTIVITY_STATUS_411_MSG(String aCTIVITY_STATUS_411_MSG) {
		ACTIVITY_STATUS_411_MSG = aCTIVITY_STATUS_411_MSG;
	}
	@Value("${activity.status.412.msg:''}")
	public void setACTIVITY_STATUS_412_MSG(String aCTIVITY_STATUS_412_MSG) {
		ACTIVITY_STATUS_412_MSG = aCTIVITY_STATUS_412_MSG;
	}
	@Value("${activity.status.414.msg:''}")
	public void setACTIVITY_STATUS_414_MSG(String aCTIVITY_STATUS_414_MSG) {
		ACTIVITY_STATUS_414_MSG = aCTIVITY_STATUS_414_MSG;
	}
	@Value("${trade.status.402.msg:''}")
	public void setTRADE_STATUS_402_MSG(String tRADE_STATUS_402_MSG) {
		TRADE_STATUS_402_MSG = tRADE_STATUS_402_MSG;
	}
	@Value("${trade.status.403.msg:''}")
	public void setTRADE_STATUS_403_MSG(String tRADE_STATUS_403_MSG) {
		TRADE_STATUS_403_MSG = tRADE_STATUS_403_MSG;
	}
	@Value("${trade.status.404.msg:''}")
	public void setTRADE_STATUS_404_MSG(String tRADE_STATUS_404_MSG) {
		TRADE_STATUS_404_MSG = tRADE_STATUS_404_MSG;
	}
	@Value("${trade.status.406.msg:''}")
	public void setTRADE_STATUS_406_MSG(String tRADE_STATUS_406_MSG) {
		TRADE_STATUS_406_MSG = tRADE_STATUS_406_MSG;
	}
	@Value("${trade.status.407.msg:''}")
	public void setTRADE_STATUS_408_MSG(String tRADE_STATUS_408_MSG) {
		TRADE_STATUS_408_MSG = tRADE_STATUS_408_MSG;
	}
	@Value("${trade.status.408.msg:''}")
	public void setTRADE_STATUS_409_MSG(String tRADE_STATUS_409_MSG) {
		TRADE_STATUS_409_MSG = tRADE_STATUS_409_MSG;
	}
	
	
	
}
