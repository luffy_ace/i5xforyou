package com.wuuxiang.i5xforyou.controller.kanjia;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.domain.Results;
import com.wuuxiang.i5xforyou.feign.impl.FeignClientService;
import com.wuuxiang.i5xforyou.service.kanjia.KanJiaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/** 
 * @ClassName: KanJiaController <br/>
 * 砍价活动controller <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午1:23:57
 *
 */
@RestController
@Api(value = "砍价操作", tags="砍价操作", protocols="https")
public class KanJiaController extends BaseController {

	@Autowired
	private FeignClientService feignClientService;
	
	@Autowired
	private KanJiaService kanJiaService;
	
	/**
	 * init <br/>
	 * 砍价活动初始化页面数据 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午1:30:30
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId 活动id
	 * @param initiatorOpenId 发起人微信用户id
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:活动初始化", notes = "说明:活动初始话接口描述")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "操作人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "campaignId", value = "活动id", required = true,dataType = "String"),
		@ApiImplicitParam(name = "initiatorOpenId", value = "活动归属人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true, dataType = "String")}
	)
	@RequestMapping(value = "/init", method = RequestMethod.GET)
	public JsonPackage init(String mpId, String openId, String campaignId, String initiatorOpenId) {
		JsonPackage jsonPackage = new JsonPackage();
		//判断入参
		if (!StringUtils.isNumeric(campaignId)) {
			//活动id为null
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage("入参不合法,campaignId=" + campaignId);
			return jsonPackage;
		}
		
		//feign获取crm
		jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询砍价活动信息
		ResultMsg resultMsg = kanJiaService.init(mpId, openId, campaignId, initiatorOpenId, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	
	/**
	 * trade <br/>
	 * 砍价活动初始化页面数据 <br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月15日 下午1:30:30
	 *
	 * @param mpId
	 * @param openId 当前用户微信id
	 * @param campaignId 活动id
	 * @param initiatorOpenId 发起人微信用户id
	 * @return JsonPackage
	 */
	
	@ApiOperation(value = "接口名称:砍价、帮砍操作", notes = "说明:砍价、帮砍操作接口描述")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "操作人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "userName", value = "操作人userName", required = true,dataType = "String"),
		@ApiImplicitParam(name = "campaignId", value = "活动id", required = true,dataType = "String"),
		@ApiImplicitParam(name = "profilePhotoImg", value = "操作人头像", required = true,dataType = "String"),
		@ApiImplicitParam(name = "initiatorOpenId", value = "活动归属人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true, dataType = "String")}
	)
	@RequestMapping(value = "/trade", method = RequestMethod.POST)
	public JsonPackage trade(String mpId, String openId,String userName, String campaignId, String initiatorOpenId, String profilePhotoImg) {
		//feign获取crm
		JsonPackage jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询砍价活动信息
		ResultMsg resultMsg = kanJiaService.trade(mpId, openId, userName, campaignId, initiatorOpenId, profilePhotoImg, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setStatus(resultMsg.getFlag());
		jsonPackage.setMessage(resultMsg.getMsg());
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * influenceRank
	 * @Description: 查询影响力排行榜
	 * @author Mobile Web Group-lff
	 * @date 2018年4月10日 下午12:56:04
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @return
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:砍价、查询影响力排行榜", notes = "说明:砍价、查询影响力排行榜接口描述")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "操作人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "campaignId", value = "活动id", required = true,dataType = "String"),
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true, dataType = "String"),
		@ApiImplicitParam(name = "initiatorOpenId", value = "发起人微信用户id", required = true, dataType = "String")}
	)
	@RequestMapping(value = "/rank/influence", method = RequestMethod.GET)
	public JsonPackage influenceRank(String mpId, String openId, String campaignId, String initiatorOpenId) {
		JsonPackage jsonPackage = new JsonPackage();
		//判断入参
		if (!StringUtils.isNumeric(campaignId)) {
			//活动id为null
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage("入参不合法,campaignId=" + campaignId);
			return jsonPackage;
		}
		
		//feign获取crm
		jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询影响力排行榜
		ResultMsg resultMsg = kanJiaService.influenceRank(mpId, openId, campaignId, initiatorOpenId, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * helperRank
	 * @Description: 查询帮砍排行榜
	 * @author Mobile Web Group-lff
	 * @date 2018年4月10日 下午12:56:04
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @return
	 * @return JsonPackage
	 */
	@ApiOperation(value = "接口名称:砍价、查询帮砍排行榜", notes = "说明:砍价、查询帮砍排行榜接口描述")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "openId", value = "操作人openid", required = true,dataType = "String"),
		@ApiImplicitParam(name = "campaignId", value = "活动id", required = true,dataType = "String"),
		@ApiImplicitParam(name = "mpId", value = "MPID", required = true, dataType = "String")}
	)
	@RequestMapping(value = "/rank/helper", method = RequestMethod.GET)
	public JsonPackage helperRank(String mpId, String openId, String campaignId) {
		JsonPackage jsonPackage = new JsonPackage();
		//判断入参
		if (!StringUtils.isNumeric(campaignId)) {
			//活动id为null
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage("入参不合法,campaignId=" + campaignId);
			return jsonPackage;
		}
		
		//feign获取crm
		jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询查询帮砍排行榜
		ResultMsg resultMsg = kanJiaService.helperRank(mpId, openId, campaignId, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
}
