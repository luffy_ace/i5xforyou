package com.wuuxiang.i5xforyou.controller.coupon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wuuxiang.i5xforyou.common.consts.Consts;
import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.common.domain.Results;
import com.wuuxiang.i5xforyou.feign.impl.FeignClientService;
import com.wuuxiang.i5xforyou.service.coupon.CouponService;
import com.wuuxiang.i5xforyou.vo.kanjia.Coupon;

/** 
 * @ClassName: CouponController <br/>
 * 砍价卷相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月16日 下午5:46:51
 *
 */
@RestController
public class CouponController extends BaseController {
	
	@Autowired
	private FeignClientService feignClientService;
	
	@Autowired
	private CouponService couponService;
	
	/**
	 * getCoupons <br/>
	 * 获取本活动下我的优惠卷列表 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 上午11:30:48
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/coupons", method = RequestMethod.GET)
	public JsonPackage getCoupons(String mpId, String openId, String campaignId) {
		//feign获取crm
		JsonPackage jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询砍价活动信息
		ResultMsg resultMsg = couponService.getCoupons(mpId, openId, campaignId, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * getCouponDetail <br/>
	 * 获取优惠卷详情 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午2:06:39
	 *
	 * @param mpId
	 * @param openId
	 * @param coupon
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/coupon/detail", method = RequestMethod.GET)
	public JsonPackage getCouponDetail(String mpId, String openId, Coupon coupon) {
		//feign获取crm
		JsonPackage jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//查询砍价活动信息
		ResultMsg resultMsg = couponService.getCouponDetail(mpId, openId, coupon, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * getCouponDetail <br/>
	 * 获取砍价赠券 <br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月19日 下午2:06:39
	 *
	 * @param mpId
	 * @param openId
	 * @param coupon
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/getgiftcoupon", method = RequestMethod.POST)
	public JsonPackage getGiftCoupon(String mpId, String openId, String initiatorOpenId,Long campaignId,String couponId) {
		//feign获取crm
		JsonPackage jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//获取砍价赠券
		ResultMsg resultMsg = couponService.getGiftCoupon(mpId, openId, initiatorOpenId, campaignId, couponId, crmVersion);
		if (resultMsg.getState() == Results.ERROR) {
			//失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	/**
	 * buyCoupon <br/>
	 * 购买优惠卷 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月21日 下午1:14:53
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @param coupon
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/coupon/buy", method = RequestMethod.POST)
	public JsonPackage buyCoupon(String mpId, String openId, String campaignId, Coupon coupon) {
		//feign获取crm
		JsonPackage jsonPackage = feignClientService.getCrmVersion(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取crm版本失败
			return jsonPackage;
		}
		String crmVersion = (String) jsonPackage.getResult();
		//feign获取crm7流水号
		String crm7Code = null;
		if (Consts.CRM7.equals(crmVersion)) {
			jsonPackage = feignClientService.getCrm7Code(mpId, openId);
			if (jsonPackage.getStatus() != 0) {
				//获取crm7流水号
				return jsonPackage;
			}
			crm7Code = String.valueOf(jsonPackage.getResult());
		}
		//feign获取小程序的appid
		jsonPackage = feignClientService.getMiniProgramInfo(mpId);
		if (jsonPackage.getStatus() != 0) {
			//获取小程序信息失败
			return jsonPackage;
		}
		JSONObject miniProgramJson = (JSONObject) JSON.toJSON(jsonPackage.getResult());
		
		//购买优惠卷
		ResultMsg resultMsg = couponService.buyCoupon(mpId, openId, campaignId, coupon, crmVersion, crm7Code, miniProgramJson.getString("cappid"));
		if (resultMsg.getState() == Results.ERROR) {
			if (-2 == resultMsg.getFlag()) {
				//库存不足失败
				jsonPackage.setStatus(-2);
				jsonPackage.setResult(resultMsg.getMsgEntity());
				return jsonPackage;
			}
			//其它失败
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(resultMsg.getException().getMessage());
			return jsonPackage;
		}
		//成功
		jsonPackage.setStatus(resultMsg.getFlag());
		jsonPackage.setResult(resultMsg.getMsgEntity());
		return jsonPackage;
	}
	
	
	
}
