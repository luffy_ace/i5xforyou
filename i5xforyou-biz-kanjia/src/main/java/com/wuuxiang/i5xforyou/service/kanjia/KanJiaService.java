package com.wuuxiang.i5xforyou.service.kanjia;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;

/** 
 * @ClassName: KanJiaService <br/>
 * 砍价活动service <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 下午1:25:24
 *
 */
public interface KanJiaService {
	
	/**
	 * init <br/>
	 * 砍价活动初始化页面数据 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午2:34:04
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId 活动id
	 * @param initiatorOpenId 发起人微信用户id
	 * @return ResultMsg
	 */
	public ResultMsg init(String mpId, String openId, String campaignId, String initiatorOpenId, String crmVersion);
	
	/**
	 * trade <br/>
	 * 砍价活动初始化页面数据 <br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月15日 下午2:34:04
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId 活动id
	 * @param initiatorOpenId 发起人微信用户id
	 * @return ResultMsg
	 */
	public ResultMsg trade(String mpId, String openId,String userName, String campaignId, String initiatorOpenId, String profilePhotoImg, String crmVersion);
	
	
	
	/**
	 * influenceRank
	 * @Description: 查询影响力排行榜
	 * @author Mobile Web Group-lff
	 * @date 2018年4月10日 下午12:54:55
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @param crmVersion
	 * @return
	 * @return ResultMsg
	 */
	public ResultMsg influenceRank(String mpId, String openId, String campaignId, String initiatorOpenId, String crmVersion);
	
	
	/**
	 * helperRank
	 * @Description: 查询帮砍排行榜
	 * @author Mobile Web Group-lff
	 * @date 2018年4月10日 下午12:54:55
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @param crmVersion
	 * @return
	 * @return ResultMsg
	 */
	public ResultMsg helperRank(String mpId, String openId, String campaignId, String crmVersion);
	
	
}
