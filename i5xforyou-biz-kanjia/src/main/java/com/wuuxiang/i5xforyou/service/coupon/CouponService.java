package com.wuuxiang.i5xforyou.service.coupon;

import com.wuuxiang.i5xforyou.common.domain.ResultMsg;
import com.wuuxiang.i5xforyou.vo.kanjia.Coupon;

/** 
 * @ClassName: CouponService <br/>
 * 砍价卷相关接口 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月16日 下午5:54:20
 *
 */
public interface CouponService {

	/**
	 * getCoupons <br/>
	 * 获取本活动下我的优惠卷列表 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 上午11:32:24
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @return ResultMsg
	 */
	public ResultMsg getCoupons(String mpId, String openId, String campaignId, String crmVersion);
	
	
	/**
	 * getCouponDetail <br/>
	 * 获取优惠卷详情 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月19日 下午2:07:22
	 *
	 * @param mpId
	 * @param openId
	 * @param coupon
	 * @return ResultMsg
	 */
	public ResultMsg getCouponDetail(String mpId, String openId, Coupon coupon, String crmVersion);
	
	/**
	 * getGiftCoupon <br/>
	 * 获取优惠卷详情 <br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月19日 下午2:07:22
	 *
	 * @param mpId
	 * @param openId
	 * @param initiatorOpenId
	 * @param campaignId
	 * @param couponId
	 * @return ResultMsg
	 */
	public ResultMsg getGiftCoupon(String mpId, String openId, String initiatorOpenId,Long campaignId,String couponId, String crmVersion);
	
	
	/**
	 * buyCoupon <br/>
	 * 购买优惠卷 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月21日 下午1:15:37
	 *
	 * @param mpId
	 * @param openId
	 * @param campaignId
	 * @param coupon
	 * @return ResultMsg
	 */
	public ResultMsg buyCoupon(String mpId, String openId, String campaignId, Coupon coupon, String crmVersion, String crm7Code, String appId);
	
}
