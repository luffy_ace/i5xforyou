package com.wuuxiang.i5xforyou.notify;

import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.domain.events.InstanceEvent;
import de.codecentric.boot.admin.server.notify.AbstractStatusChangeNotifier;
import reactor.core.publisher.Mono;

/**
 * 
 * @ClassName: CustomNotifier
 * @Description: 自定义提醒
 * @author Mobile Web Group-lx
 * @date 2018年5月2日 下午1:38:17
 *
 */
public class CustomNotifier extends AbstractStatusChangeNotifier {

	public CustomNotifier(InstanceRepository repository) {
		super(repository);
	}

	@Override
	protected Mono<Void> doNotify(InstanceEvent event, Instance instance) {
		return Mono.fromRunnable(() -> {
			
			/*System.out.println(event.getInstance().getValue()); //ecd41a2d9f6f
			System.out.println(event.getTimestamp()); //2018-05-02T05:23:11.929Z
			System.out.println(event.getType()); //STATUS_CHANGED
			System.out.println(event.getVersion()); //1
			
			System.out.println(instance.getBuildVersion());//null
			System.out.println(instance.getId());//ecd41a2d9f6f
			System.out.println(instance.getInfo().getValues().toString());//{}
			System.out.println(instance.getStatusInfo().getStatus());//DOWN
			System.out.println(instance.getStatusInfo().getDetails().toString());//{error=Not Found, status=404}
			System.out.println(instance.getStatusTimestamp());//2018-05-02T05:23:11.929Z
			System.out.println(instance.getVersion()); //1
			
			System.out.println(instance.getRegistration().getName()); //I5XFORYOU-SUPPORT-SERVICE-TESTCLIENT
			System.out.println(instance.getRegistration().getHealthUrl()); //http://192.168.14.187:31001/actuator/health
			System.out.println(instance.getRegistration().getServiceUrl()); //http://192.168.14.187:31001/
			System.out.println(instance.getRegistration().getManagementUrl()); //http://192.168.14.187:31001/actuator
			System.out.println(instance.getRegistration().getSource()); //discovery
			System.out.println(instance.getRegistration().getMetadata()); //{management.port=31001, context-path=/test, jmx.port=49456, zone=""}
			 */			

		});
	}

}
