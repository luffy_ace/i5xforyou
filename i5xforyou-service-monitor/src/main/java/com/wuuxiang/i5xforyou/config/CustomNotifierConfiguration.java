package com.wuuxiang.i5xforyou.config;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wuuxiang.i5xforyou.notify.CustomNotifier;

import de.codecentric.boot.admin.server.config.AdminServerNotifierAutoConfiguration.CompositeNotifierConfiguration;
import de.codecentric.boot.admin.server.config.AdminServerNotifierAutoConfiguration.NotifierTriggerConfiguration;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;

/**
 * 
 * @ClassName: CustomNotifierConfiguration
 * @Description: 自定义提醒
 * @author Mobile Web Group-lx
 * @date 2018年5月2日 下午1:38:31
 *
 */
@Configuration
@AutoConfigureBefore({NotifierTriggerConfiguration.class, CompositeNotifierConfiguration.class})
public class CustomNotifierConfiguration {
	
	@Bean
	@ConditionalOnMissingBean
    @ConfigurationProperties("spring.boot.admin.notify.custom")
    public CustomNotifier customNotifier(InstanceRepository repository) {
        return new CustomNotifier(repository);
    }

}
