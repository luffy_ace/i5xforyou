package com.wuuxiang.i5xforyou.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/** 
 * @ClassName: MonitorWebMvcConfiguration <br/>
 * 静态资源映射 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月27日 下午5:27:57
 *
 */
@Configuration
public class MonitorWebMvcConfiguration extends WebMvcConfigurationSupport {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//配置静态资源处理
		registry.addResourceHandler("/hystrix/**")
				.addResourceLocations("classpath:/static/hystrix/");
		
		registry.addResourceHandler("/assets/**")
		.addResourceLocations("classpath:/META-INF/spring-boot-admin-server-ui/assets/");
		
		registry.addResourceHandler("/webjars/**")
		.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
}
