package com.wuuxiang.i5xforyou.auth.vo;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.wuuxiang.i5xforyou.utils.JwtUtil;

public class AuthResult implements Serializable {
	private static final long serialVersionUID = -5115643489738605990L;
	private String openId;
	private String unionId;
	private String token;
	private long exp = JwtUtil.exp;
	public AuthResult() {
		super();
	}
	public AuthResult(String openId, String unionId) {
		super();
		this.openId = openId;
		this.unionId = unionId;
	}
	public String getopenId() {
		return openId;
	}
	public void setopenId(String openId) {
		this.openId = openId;
	}
	public String getunionId() {
		return unionId;
	}
	public void setunionId(String unionId) {
		this.unionId = unionId;
	}
	public String getToken() throws Exception {
		/*if(StringUtils.isNotBlank(openId)) {
			unionId = (unionId == null )?"":unionId;
			token = JwtUtil.createToken(openId + unionId);
		}*/
		if(StringUtils.isNotBlank(openId)) {
			token = JwtUtil.createToken(openId);
		}
		return token;
	}
	public long getExp() {
		return exp;
	}
	
//	public static void main(String[] args) {
//		new AuthResult("", null);
//	}
}
