package com.wuuxiang.i5xforyou.auth.service;

import com.wuuxiang.i5xforyou.auth.vo.WxComponent;
import com.wuuxiang.i5xforyou.auth.vo.WxMiniProgram;
import com.wuuxiang.i5xforyou.common.service.BaseService;

public interface AuthService extends BaseService {
	
	/**
	 * getWxMiniProgram
	 * @Description: 获取小程序相关信息
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月15日 下午5:15:36
	 * @param appid
	 * @return WxMiniProgram
	 */
	WxMiniProgram getWxMiniProgram(String cmpid);
	/**
	 * getWxComponent
	 * @Description: 获取第三方平台相关信息
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月15日 下午5:15:36
	 * @param appid
	 * @return WxComponent
	 */
	WxComponent getWxComponent(String component_appid);
}
