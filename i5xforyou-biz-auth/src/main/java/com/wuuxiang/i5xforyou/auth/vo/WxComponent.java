package com.wuuxiang.i5xforyou.auth.vo;
import java.io.Serializable;
import java.util.Date;
/**
 * @ClassName: WxComponent
 * @Description: 第三方平台信息
 * @author Mobile Web Group-LiuYandong
 * @date 2018年3月15日 上午10:15:31
 *
 */
public class WxComponent implements Serializable {
	private static final long serialVersionUID = -8842071851190618460L;
	private String component_appid;
	private String component_verify_ticket;
	private String component_access_token;
	private Date ticket_updata_date;
	private Date access_token_updata_date;
	public String getComponent_appid() {
		return component_appid;
	}
	public void setComponent_appid(String component_appid) {
		this.component_appid = component_appid;
	}
	public String getComponent_verify_ticket() {
		return component_verify_ticket;
	}
	public void setComponent_verify_ticket(String component_verify_ticket) {
		this.component_verify_ticket = component_verify_ticket;
	}
	public String getComponent_access_token() {
		return component_access_token;
	}
	public void setComponent_access_token(String component_access_token) {
		this.component_access_token = component_access_token;
	}
	public Date getTicket_updata_date() {
		return ticket_updata_date;
	}
	public void setTicket_updata_date(Date ticket_updata_date) {
		this.ticket_updata_date = ticket_updata_date;
	}
	public Date getAccess_token_updata_date() {
		return access_token_updata_date;
	}
	public void setAccess_token_updata_date(Date access_token_updata_date) {
		this.access_token_updata_date = access_token_updata_date;
	}
	

}
