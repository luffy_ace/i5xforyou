package com.wuuxiang.i5xforyou.auth.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wuuxiang.i5xforyou.auth.service.AuthService;
import com.wuuxiang.i5xforyou.auth.vo.WxComponent;
import com.wuuxiang.i5xforyou.auth.vo.WxMiniProgram;
import com.wuuxiang.i5xforyou.common.service.impl.BaseServiceImpl;
import com.wuuxiang.jdbc.spring.boot.autoconfigure.dao.O2OJdbcRepository;
@Service("authService")
public class AuthServiceImpl extends BaseServiceImpl implements AuthService {
	
	@Autowired
	private O2OJdbcRepository<WxMiniProgram> o2oMini;
	@Autowired
	private O2OJdbcRepository<WxComponent> o2oWxComponent;
	
	/**
	 * getWxMiniProgram
	 * @Description: 获取小程序相关信息
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年3月15日 下午5:15:36
	 * @param appid
	 * @return WxMiniProgram
	 */
	@Override
	public WxMiniProgram getWxMiniProgram(String cmpid) {
		String sql = "select wmp.cappid,wmp.cmpid,wmp.caccesstoken from wxminiprogram wmp where wmp.cmpid = ? and wmp.iauthflg =1 and wmp.ideleteflg = 0"; 
		return o2oMini.getJavaBean(sql, WxMiniProgram.class, cmpid);
	}
	
	@Override
	public WxComponent getWxComponent(String component_appid) {
		String sql = "SELECT c.component_appid,c.component_verify_ticket,c.component_access_token,c.ticket_updata_date,c.access_token_updata_date FROM wx_component c WHERE c.component_appid = ?";
		return o2oWxComponent.getJavaBean(sql, WxComponent.class, component_appid);

	}
	

}
