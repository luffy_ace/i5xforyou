package com.wuuxiang.i5xforyou.auth.consts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: Consts <br/>
 * 共通变量，对应i5xforyou-auth-conf中的变量 <br/>
 * @author Mobile Web Group-LiuYandong
 * @date 2018年3月15日 上午8:39:59
 *
 */
@Component
@RefreshScope
public class AuthConsts {
	/*
	 * 重要，对静态static变量通过@value赋值，要通过非静态的set方法才行
	 */
	/* 公众号第三方授权商配置信息  */
	/* 公众号第三方平台AppID */
	public static String COMPONENT_APPID = "";
	
	/* 微信开放平台代小程序登录地址 */
	/* https://api.weixin.qq.com/sns/component/jscode2session
	 * ?appid=APPID&js_code=JSCODE
	 * &grant_type=authorization_code
	 * &component_appid=COMPONENT_APPID
	 * &component_access_token=ACCESS_TOKEN
	*/
	public static String COMPONENT_JSCODE2SESSION_URL = "";
	
	@Value("${component.appid:''}")
	public void setCOMPONENT_APPID(String cOMPONENT_APPID) {
		COMPONENT_APPID = cOMPONENT_APPID;
	}
	@Value("${http.component.jscode2session.url:''}")
	public void setCOMPONENT_JSCODE2SESSION_URL(String cOMPONENT_JSCODE2SESSION_URL) {
		COMPONENT_JSCODE2SESSION_URL = cOMPONENT_JSCODE2SESSION_URL;
	}
}
