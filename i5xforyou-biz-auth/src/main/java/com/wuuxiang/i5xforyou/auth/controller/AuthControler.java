package com.wuuxiang.i5xforyou.auth.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.auth.consts.AuthConsts;
import com.wuuxiang.i5xforyou.auth.service.AuthService;
import com.wuuxiang.i5xforyou.auth.vo.AuthResult;
import com.wuuxiang.i5xforyou.auth.vo.Jscode2session;
import com.wuuxiang.i5xforyou.auth.vo.WxComponent;
import com.wuuxiang.i5xforyou.auth.vo.WxMiniProgram;
import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;
import com.wuuxiang.i5xforyou.utils.HttpUtil;
//import com.wuuxiang.jdbc.spring.boot.autoconfigure.dao.O2OJdbcRepository;
/**
 * @ClassName: AuthControler
 * @Description: 获取用户授权信息授权
 * @author Mobile Web Group-lff
 * @date 2017年9月6日 上午10:15:31
 *
 */
@RestController
public class AuthControler extends BaseController {
	@Autowired
	private AuthService authService;
	/**
	 * login(登录)
	 * @Description: 登录
	 * @author Mobile Web Group-lyd
	 * @date 2017年9月6日 上午10:33:40
	 *
	 * @param mcId
	 * @param password
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public JsonPackage login(String code,String mpid){
		JsonPackage jsonPackage = new JsonPackage();
		try {
			// 获取小程序配置信息
			WxMiniProgram wxMiniProgram = authService.getWxMiniProgram(mpid);
			if(wxMiniProgram == null) {
				jsonPackage.setStatus(-1);
				jsonPackage.setMessage("未能获取到小程序配置信息");
				return jsonPackage;
			}
			// 获取第三方平台信息
			WxComponent wxComponent = authService.getWxComponent(AuthConsts.COMPONENT_APPID);
			if(wxComponent == null) {
				jsonPackage.setStatus(-1);
				jsonPackage.setMessage("未能获取到开放平台配置信息");
				return jsonPackage;
			}
			String url = AuthConsts.COMPONENT_JSCODE2SESSION_URL;
			url = url.replace("COMPONENT_APPID", AuthConsts.COMPONENT_APPID);
			url = url.replace("APPID", wxMiniProgram.getCappid());
			url = url.replace("JSCODE", code);
			url = url.replace("ACCESS_TOKEN", wxComponent.getComponent_access_token());
			Jscode2session jscode2session = null;
			//授权
			jscode2session = HttpUtil.doGetObject(url, Jscode2session.class);
			if(StringUtils.isNotBlank(jscode2session.getOpenid())) {
				//创造吾享自身Token
				AuthResult authResult = new AuthResult(jscode2session.getOpenid(),jscode2session.getUnionid());
				jsonPackage.setResult(authResult);
			}else {
				jsonPackage.setStatus(-1);
				jsonPackage.setMessage(jscode2session.getErrmsg());
				// TODO 记录日志待补充
			}
		} catch (Exception e) {
			// TODO 记录日志待补充
			e.printStackTrace();
			jsonPackage.setStatus(-1);
			jsonPackage.setMessage(e.getMessage());
		}
		return jsonPackage;
	}
	
	/**
	 * getMiniProgramInfo <br/>
	 * 获取小程序的基本信息 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月22日 上午9:14:44
	 *
	 * @param mpId
	 * @return JsonPackage
	 */
	@RequestMapping(value = "/miniprogram/info", method = RequestMethod.GET)
	public JsonPackage getMiniProgramInfo(String mpId) {
		JsonPackage jsonPackage = new JsonPackage();
		jsonPackage.setResult(authService.getWxMiniProgram(mpId));
		return jsonPackage;
	}
	
	/**
	 * getComponentToken <br/>
	 * 获取开放平台第三方token<br/>
	 *
	 * @author Mobile Web Group-LiuYandong
	 * @date 2018年4月08日 上午9:14:44
	 * @return String
	 */
	@RequestMapping(value = "/miniprogram/getComponentToken", method = RequestMethod.GET)
	public String getComponentToken() {
		
		WxComponent wxComponent = authService.getWxComponent(AuthConsts.COMPONENT_APPID);
		if(wxComponent != null) {
			return wxComponent.getComponent_access_token();
		}
		return "";
	}
	
	
}
