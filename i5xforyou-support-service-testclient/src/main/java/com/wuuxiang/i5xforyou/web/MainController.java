package com.wuuxiang.i5xforyou.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.bean.User;
import com.wuuxiang.jdbc.spring.boot.autoconfigure.dao.CRMJdbcRepository;
import com.wuuxiang.jdbc.spring.boot.autoconfigure.dao.O2OJdbcRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RefreshScope
@Api(value = "测试示例", tags="点餐请求", protocols="https")
@RequestMapping(value = "/bbb")
public class MainController {

	@Value("${ceshi1}")
	private String ceshi1;

	/*
	@Value("${test.ceshi1}")
	private String ceshi2;

	@Value("${o2ousername}")
	private String o2ousername;*/

	@Autowired
	private O2OJdbcRepository<Integer> o2oInteger;

	@Autowired
	private CRMJdbcRepository<Integer> crmInteger;

/*	@Autowired
	private RedisTemplate<String, Long> countTemplate;*/

	@ApiOperation(value = "接口名称:连接O2O数据源示例", notes = "说明:获取wxlogin的数量")
	@RequestMapping(value = "/o2o", method = RequestMethod.GET)
	public String o2o(){
		
		log.trace("trace-o2o");
		log.debug("debug-o2o");
		log.info("info-o2o");
		log.warn("warn-o2o");
		log.error("error-o2o");
		
		String countSQL = "SELECT COUNT(*) FROM WXLOGIN WL WHERE WL.ISTATUS = ?";
		Long count = o2oInteger.getCount(countSQL, 1);
		System.out.println(count);
		return String.valueOf(count);
	}

	@ApiOperation(value = "接口名称:连接CRM数据源示例", notes = "说明:获取wxlogin的数量", consumes="application/xml", produces="application/xml")
	@RequestMapping(value = "/crm", method = RequestMethod.GET)
	public String crm(){
		String countSQL = "SELECT COUNT(*) FROM CARD C WHERE C.CARD_STATUS = ?";
		Long count = crmInteger.getCount(countSQL, 12510);
		System.out.println(count);
		return String.valueOf(count);
	}

	@ApiIgnore
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "Hello World!" + ceshi1 ;
	}

	@ApiOperation(value = "获取用户详细信息", notes = "根据url的id来获取用户详细信息")
	@ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "path", dataType = "Long")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String getUser(@PathVariable Long id) {
		return "获取用户详细信息!" + ceshi1;
	}

	@ApiOperation(value = "更新用户详细信息", notes = "根据url的id来指定更新对象，并根据传过来的user信息来更新用户详细信息")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "path", dataType = "Long"),
			@ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "User") })
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public String putUser(@PathVariable Long id, @RequestBody User user) {
		return "success";
	}

}
