package com.wuuxiang.i5xforyou.configuration;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.publisher.Mono;

/** 
 * @ClassName: GatewayConfiguration <br/>
 * gateway的配置Configuration <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月22日 上午10:06:28
 *
 */
@Configuration
public class GatewayConfiguration {

	@Bean(name="openIdKeyResolver")
	public KeyResolver openIdKeyResolver() {
		System.out.println("openIdKeyResolver" );
		//根据openId来区别同一用户
		return exchange -> {
			System.out.println("openIdKeyResolver:" + exchange.getRequest().getQueryParams().getFirst("openId"));
			return Mono.just(exchange.getRequest().getQueryParams().getFirst("openId"));
		};
	}
	
}
