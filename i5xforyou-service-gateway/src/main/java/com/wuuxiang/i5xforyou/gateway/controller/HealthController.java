package com.wuuxiang.i5xforyou.gateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** 
 * @ClassName: HealthController <br/>
 * 服务启动检测controller <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 上午9:11:32
 *
 */
@RestController
public class HealthController {
	
	@RequestMapping(value = {"/","/i5xforyou"})
	public String hello() {
		return "Hello i5xforyou!";
	}
	
}
