package com.wuuxiang.i5xforyou.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: JwtCheckGatewayFilterFactory <br/>
 * JWT校验 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月6日 上午11:02:42
 *
 */
@Component
public class JwtCheckGatewayFilterFactory extends AbstractGatewayFilterFactory<Object> {
	
	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			return chain.filter(exchange);
			/*String token = exchange.getRequest().getHeaders().getFirst("Authorization");
			String openId =  exchange.getRequest().getQueryParams().getFirst("openId");
			//check token
			if (StringUtils.isNotBlank(token)) {
				String tokenOpenID = JwtUtil.verifyToken(token);
				if(StringUtils.isNotBlank(tokenOpenID)) {
					if(openId != null) {
						if(openId.equals(tokenOpenID)) {
							return chain.filter(exchange);
						}
					} else {
						return chain.filter(exchange);
					}
				}
				
			}
			
			//不合法
			ServerHttpResponse response = exchange.getResponse();
			//设置headers
			HttpHeaders httpHeaders = response.getHeaders();
			httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
			httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
			//设置body
			JsonPackage jsonPackage = new JsonPackage();
			jsonPackage.setStatus(110);
			jsonPackage.setMessage("未登录或登录超时");
			DataBuffer bodyDataBuffer = response.bufferFactory().wrap(jsonPackage.toJSONString().getBytes());
			
			return response.writeWith(Mono.just(bodyDataBuffer));*/
		};
	}
	
}
