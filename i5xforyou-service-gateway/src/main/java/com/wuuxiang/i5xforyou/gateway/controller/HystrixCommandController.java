package com.wuuxiang.i5xforyou.gateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.wuuxiang.i5xforyou.common.domain.JsonPackage;

/** 
 * @ClassName: HealthController <br/>
 * 服务启动检测controller <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 上午9:11:32
 *
 */
@RestController
public class HystrixCommandController {
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping("/hystrixTimeout")
	public JsonPackage hystrixTimeout() {
		log.error("i5xforyou-service-gateway触发了断路由");
		return JsonPackage.getHystrixJsonPackage();
	}
	
	@HystrixCommand(commandKey="authHystrixCommand")
	public JsonPackage authHystrixCommand() {
		return JsonPackage.getHystrixJsonPackage();
	}
	
	@HystrixCommand(commandKey="crmHystrixCommand")
	public JsonPackage crmHystrixCommand() {
		return JsonPackage.getHystrixJsonPackage();
	}
	
	@HystrixCommand(commandKey="kanjiaHystrixCommand")
	public JsonPackage kanjiaHystrixCommand() {
		return JsonPackage.getHystrixJsonPackage();
	}
	
	@HystrixCommand(commandKey="ilookHystrixCommand")
	public JsonPackage ilookHystrixCommand() {
		return JsonPackage.getHystrixJsonPackage();
	}
	
	
}
