package com.wuuxiang.i5xforyou.gateway;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;

import com.wuuxiang.i5xforyou.utils.HttpUtil;

/** 
 * @ClassName: Test <br/>
 * Test <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月22日 下午5:11:04
 *
 */
public class Test {

	public static void main(String[] args) {
		ExecutorService publishThreadPool = Executors.newFixedThreadPool(15);//发送消息的线程池
		for (int i = 0; i < 15; i++) {
			publishThreadPool.execute(new Runnable() {
				
				@Override
				public void run() {
					String a;
					try {
						a = HttpUtil.doGet("http://localhost:30020/i5xforyou/crm/common/crmversion?mpId=gh_d46dcc0204fe&openId=abc");
						if (StringUtils.isBlank(a )) {
							System.err.println("-----------------------------------------------");
							
						} else {
							System.out.println(a);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

	}

}
