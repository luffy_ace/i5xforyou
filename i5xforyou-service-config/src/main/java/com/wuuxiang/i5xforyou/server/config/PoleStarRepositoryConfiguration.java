package com.wuuxiang.i5xforyou.server.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.wuuxiang.i5xforyou.server.config.environment.PoleStarEnvironmentRepository;
import com.wuuxiang.i5xforyou.server.config.properties.PoleStarConfigProperties;

@Configuration
@ConditionalOnMissingBean({EnvironmentRepository.class})
@ConditionalOnClass({PoleStarConfigProperties.class})
@Profile("polestar")
public class PoleStarRepositoryConfiguration {

    // @Autowired
    // private ConfigurableEnvironment environment;

    /*
	@Bean
    public SearchPathLocator searchPathLocator() {
        return new NativeEnvironmentRepository(environment);
    }*/

    @Bean
    public PoleStarEnvironmentRepository poleStarEnvironmentRepository() {
        return new PoleStarEnvironmentRepository();
    }

   /* @Autowired
	private PoleStarConfigProperties poleStarConfigProperties;

	@Bean
	public ZookeeperSurPassConfigProfile getConfigProfile() {
		return new ZookeeperSurPassConfigProfile(poleStarConfigProperties.getAddress(),
				poleStarConfigProperties.getTeamName(), poleStarConfigProperties.getLoadDef());
	}

	@Bean(name = "configGroup")
	public ConfigGroup getConfigGroup(ZookeeperSurPassConfigProfile configProfile) {
		return new ZookeeperSurPassConfigGroup(configProfile, "");
	}*/


}
