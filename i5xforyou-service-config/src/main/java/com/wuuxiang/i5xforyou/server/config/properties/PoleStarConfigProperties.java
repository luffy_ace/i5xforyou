package com.wuuxiang.i5xforyou.server.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @ClassName: PoleStarConfigProperties
 * @Description: PoleStar连接配置类
 * @author Mobile Web Group-lx
 * @date 2018年3月1日 上午9:31:55
 *
 *       polestar: address:
 *       192.168.14.231:2181,192.168.14.232:2181,192.168.14.233:2181 teamName:
 *       /WECHAT loadDef: true nodes: i5xwxplus-conf
 *
 */
@Configuration
@ConfigurationProperties(prefix = "spring.cloud.config.server.polestar")
public class PoleStarConfigProperties {

	private String address;

	private String teamName;

	private Boolean loadDef;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Boolean getLoadDef() {
		return loadDef;
	}

	public void setLoadDef(Boolean loadDef) {
		this.loadDef = loadDef;
	}

}
