package com.wuuxiang.i5xforyou.server.config.service;

import java.util.List;

import org.springframework.cloud.config.environment.PropertySource;

public interface PoleStarConfigRepositoryService {

	public List<PropertySource> getConfigPropertySources(String application, String profile, String label);
}
