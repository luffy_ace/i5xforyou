package com.wuuxiang.i5xforyou.server.config.environment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.util.StringUtils;

import com.wuuxiang.i5xforyou.server.config.service.PoleStarConfigRepositoryService;

public class PoleStarEnvironmentRepository implements EnvironmentRepository {

	@Autowired
	private PoleStarConfigRepositoryService poleStarConfigRepositoryService;

	@Override
	public Environment findOne(String application, String profile, String label) {

		List<PropertySource> propertySources = poleStarConfigRepositoryService
				.getConfigPropertySources(application, profile, label);

		if (propertySources != null && !propertySources.isEmpty()) {
			Environment environment = new Environment(application, StringUtils.commaDelimitedListToStringArray(profile), label, null, null);
			for (PropertySource propertySource : propertySources) {
				if (!propertySource.getSource().isEmpty()) {
					environment.add(propertySource);
				}
			}
			return environment;
		}
		return new Environment(application, profile);
	}

}
