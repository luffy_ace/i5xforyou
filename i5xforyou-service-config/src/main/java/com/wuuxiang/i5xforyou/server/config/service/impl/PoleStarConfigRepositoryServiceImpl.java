package com.wuuxiang.i5xforyou.server.config.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.wuuxiang.i5xforyou.server.config.properties.PoleStarConfigProperties;
import com.wuuxiang.i5xforyou.server.config.service.PoleStarConfigRepositoryService;
import com.wuuxiang.polestar.core.zookeeper.surpass.ZookeeperSurPassConfigGroup;
import com.wuuxiang.polestar.core.zookeeper.surpass.ZookeeperSurPassConfigProfile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("poleStarConfigRepositoryService")
public class PoleStarConfigRepositoryServiceImpl implements PoleStarConfigRepositoryService {

	@Autowired
	private PoleStarConfigProperties poleStarConfigProperties;

	@Override
	public List<PropertySource> getConfigPropertySources(String application, String profile, String label) {

		// System.out.println("application:" + application); // application
		// System.out.println("profile:" + profile); // dev,test
		// System.out.println("label:" + label); // master

		String teamNameValue = poleStarConfigProperties.getTeamName();
		if (StringUtils.isEmpty(teamNameValue)) {
			teamNameValue = "default";
		}

		// 获取团队列表 默认 default和自定义Team，默认配置里写的是WECHAT,相当于加载default和WECHAT
		List<String> teamNames = new ArrayList<String>(
				new LinkedHashSet<>(Arrays.asList(StringUtils.commaDelimitedListToStringArray(teamNameValue))));

		List<String> applications = new ArrayList<String>(
				new LinkedHashSet<>(Arrays.asList(StringUtils.commaDelimitedListToStringArray(application))));

		List<PropertySource> propertySources = new ArrayList<PropertySource>();

		long start = System.nanoTime();
		for (String teamName : teamNames) {
			// 默认加载application.yml里的 polestar.teamName WECHAT项目组
			ZookeeperSurPassConfigProfile zookeeperSurPassConfigProfile = new ZookeeperSurPassConfigProfile(
					poleStarConfigProperties.getAddress(), teamName, poleStarConfigProperties.getLoadDef());
			for (String applicationNode : applications) {
				ZookeeperSurPassConfigGroup configGroup = new ZookeeperSurPassConfigGroup(zookeeperSurPassConfigProfile,
						applicationNode);
				log.info(teamName + ":" + applicationNode + ",new ZookeeperSurPassConfigGroup耗时"
						+ (TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)) + "毫秒");
				if (configGroup != null && !configGroup.isEmpty()) {
					PropertySource propertySource = new PropertySource(teamName + ":" + applicationNode, configGroup);
					propertySources.add(propertySource);
				}
				try {
					configGroup.close();
					configGroup = null;
				} catch (IOException e) {
					log.error("getConfigPropertySources错误" + e.toString());
				}
			}
		}
		return propertySources;
	}

	/*
	 * @Override public List<PropertySource> getConfigPropertySources(String
	 * application, String profile, String label) {
	 * 
	 * // System.out.println("application:" + application); // application //
	 * System.out.println("profile:" + profile); // dev,test //
	 * System.out.println("label:" + label); // master
	 * 
	 * String teamNameValue = poleStarConfigProperties.getTeamName(); if
	 * (StringUtils.isEmpty(teamNameValue)) { teamNameValue = "default"; }
	 * 
	 * // 获取团队列表 默认 default和自定义Team，默认配置里写的是WECHAT,相当于加载default和WECHAT List<String>
	 * teamNames = new ArrayList<String>( new
	 * LinkedHashSet<>(Arrays.asList(StringUtils.commaDelimitedListToStringArray(
	 * teamNameValue))));
	 * 
	 * List<String> applications = new ArrayList<String>( new
	 * LinkedHashSet<>(Arrays.asList(StringUtils.commaDelimitedListToStringArray(
	 * application))));
	 * 
	 * List<PropertySource> propertySources = new ArrayList<PropertySource>();
	 * 
	 * long start = System.nanoTime(); for (String teamName : teamNames) { //
	 * 默认加载application.yml里的 polestar.teamName WECHAT项目组
	 * ZookeeperSurPassConfigProfile zookeeperSurPassConfigProfile = new
	 * ZookeeperSurPassConfigProfile( poleStarConfigProperties.getAddress(),
	 * teamName, poleStarConfigProperties.getLoadDef());
	 * 
	 * ConcurrentHashMap<String, ZookeeperSurPassConfigGroup> zkConfigGroupMap; if
	 * (zkExistsConfigGroupMap.containsKey(teamName)) { zkConfigGroupMap =
	 * zkExistsConfigGroupMap.get(teamName); if (zkConfigGroupMap == null ) {
	 * zkConfigGroupMap = new ConcurrentHashMap<String,
	 * ZookeeperSurPassConfigGroup>(applications.size()); } for (String
	 * applicationNode : applications) { if
	 * (zkConfigGroupMap.containsKey(applicationNode)) { ZookeeperSurPassConfigGroup
	 * configGroup = zkConfigGroupMap.get(applicationNode); if (configGroup != null
	 * && !configGroup.isEmpty()) { PropertySource propertySource = new
	 * PropertySource(teamName + ":" + applicationNode, configGroup);
	 * propertySources.add(propertySource); } } else { // 节点不存在
	 * ZookeeperSurPassConfigGroup configGroup = new
	 * ZookeeperSurPassConfigGroup(zookeeperSurPassConfigProfile, applicationNode);
	 * log.info(teamName + ":" + applicationNode +
	 * ",new ZookeeperSurPassConfigGroup耗时" +
	 * (TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)) + "毫秒"); if
	 * (configGroup != null && !configGroup.isEmpty()) { PropertySource
	 * propertySource = new PropertySource(teamName + ":" + applicationNode,
	 * configGroup); propertySources.add(propertySource); }
	 * zkConfigGroupMap.put(applicationNode, configGroup); } } } else { //
	 * TEAM不存在节点定不存在 zkConfigGroupMap = new ConcurrentHashMap<String,
	 * ZookeeperSurPassConfigGroup>(applications.size()); for (String
	 * applicationNode : applications) { ZookeeperSurPassConfigGroup configGroup =
	 * new ZookeeperSurPassConfigGroup(zookeeperSurPassConfigProfile,
	 * applicationNode); log.info(teamName + ":" + applicationNode +
	 * ",new ZookeeperSurPassConfigGroup耗时" +
	 * (TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)) + "毫秒"); if
	 * (configGroup != null && !configGroup.isEmpty()) { PropertySource
	 * propertySource = new PropertySource(teamName + ":" + applicationNode,
	 * configGroup); propertySources.add(propertySource); }
	 * zkConfigGroupMap.put(applicationNode, configGroup); }
	 * zkExistsConfigGroupMap.put(teamName, zkConfigGroupMap); } } return
	 * propertySources; }
	 */
}
