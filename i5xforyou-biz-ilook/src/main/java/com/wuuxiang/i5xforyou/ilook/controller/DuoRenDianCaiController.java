package com.wuuxiang.i5xforyou.ilook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.ilook.service.DuoRenDianCaiService;
import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;

/** 
 * @ClassName: DuoRenDianCaiController
 * @Description: 多人点餐
 * @author Mobile Web Group-lff
 * @date 2018年4月26日 上午11:29:03
 *
 */
@RestController
public class DuoRenDianCaiController extends BaseController {
	
	@Autowired
	private DuoRenDianCaiService duoRenDianCaiService;
	
	//获取提前下单标志
	@RequestMapping(value="/duorendiancai/beforeSubmitFlg", method=RequestMethod.GET) 
	public ILookResult getBeforeSubmitFlg(String mcId, String tableNo, String openId) {
		
		return duoRenDianCaiService.getBeforeSubmitFlg(mcId, tableNo, openId);
	}

}
