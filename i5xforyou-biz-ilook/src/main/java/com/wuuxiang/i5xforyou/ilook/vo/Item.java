package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wuuxiang.i5xforyou.ilook.util.ValidatorUtil;

public class Item {
	private String itemClassId; // 小类ID
	private String itemClassName; //小类名称
	private String itemId; // 品项ID
	private String name; // 品项名称

	private String stdPrice; // 标准价
	private String vipPrice; // 会员价

	private String unitName; // 单位
	private String taFileName; // 图片

	private String hyjFlg; // 会员价开启标记
	private String mljFlg; // 满立减标记
	private String gqFlg; // 沽清标记

	//private String promPrice; // 促销价
	private String newFlg; // 新菜标志
	private String recmdFlg; // 推荐菜标志
	private String setMealFlg; // 套餐标记
	private String desp; // 描述

	private String boxPrice; //餐盒费
	private Integer minSel; //最小点餐量
	private Integer maxSel; //最大点餐量

	private String[] itemTag;

	private String code; //菜品CODE
	private Integer isSale = 1;//分时段是否售卖，1售卖，0不售卖

	private Integer limitCounts; //库存限量
	private Integer leftCounts;//剩余数量
	
	private List<ItemMulitUnit> itemMulitUnits; //菜品多单位
	
	private Integer spicy; //菜品辣度
	
	private Integer show = 1; // 0 不显示  1显示
	
	private Integer dishType = 0; // 0常规菜   1主菜  2 推荐菜

	public String getItemClassName() {
		return itemClassName;
	}

	public void setItemClassName(String itemClassName) {
		this.itemClassName = itemClassName;
	}



	public Integer getIsSale() {
		return isSale;
	}

	public void setIsSale(Integer isSale) {
		this.isSale = isSale;
	}

	public List<ItemMulitUnit> getItemMulitUnits() {
		return itemMulitUnits;
	}

	public void setItemMulitUnits(List<ItemMulitUnit> itemMulitUnits) {
		this.itemMulitUnits = itemMulitUnits;
	}

	public String getItemClassId() {
		return itemClassId;
	}

	public void setItemClassId(String itemClassId) {
		this.itemClassId = itemClassId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStdPrice() {
		return stdPrice;
	}

	public void setStdPrice(String stdPrice) {
		this.stdPrice = stdPrice;
	}

	public String getVipPrice() {
		return vipPrice;
	}

	public void setVipPrice(String vipPrice) {
		this.vipPrice = vipPrice;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getTaFileName() {
		if(StringUtils.isBlank(taFileName)||!ValidatorUtil.isUrl(taFileName)) {
			return "//cdnpic.wuuxiang.com/pic/merchant/nopic/nopic.jpg";
		}
		return taFileName;
	}

	public void setTaFileName(String taFileName) {
		this.taFileName = taFileName;
	}

	public String getHyjFlg() {
		return hyjFlg;
	}

	public void setHyjFlg(String hyjFlg) {
		this.hyjFlg = hyjFlg;
	}

	public String getMljFlg() {
		return mljFlg;
	}

	public void setMljFlg(String mljFlg) {
		this.mljFlg = mljFlg;
	}

	public String getGqFlg() {
		return gqFlg;
	}

	public void setGqFlg(String gqFlg) {
		this.gqFlg = gqFlg;
	}

	public String getNewFlg() {
		return newFlg;
	}

	public void setNewFlg(String newFlg) {
		this.newFlg = newFlg;
	}

	public String getRecmdFlg() {
		return recmdFlg;
	}

	public void setRecmdFlg(String recmdFlg) {
		this.recmdFlg = recmdFlg;
	}

	public String getSetMealFlg() {
		return setMealFlg;
	}

	public void setSetMealFlg(String setMealFlg) {
		this.setMealFlg = setMealFlg;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	public String getBoxPrice() {
		return boxPrice;
	}

	public void setBoxPrice(String boxPrice) {
		this.boxPrice = boxPrice;
	}

	public Integer getMinSel() {
		return minSel;
	}

	public void setMinSel(Integer minSel) {
		//为空时，未设置则默认最小点餐量1
		if (minSel == null || minSel == 0) {
			this.minSel = 1;
		} else {
			this.minSel = minSel;
		}
	}

	public Integer getMaxSel() {
		return maxSel;
	}

	public void setMaxSel(Integer maxSel) {
		//为空时，未设置则默认最大点单量Integer.MAX_VALUE
		if (maxSel == null || maxSel == 0) {
			this.maxSel = Integer.MAX_VALUE;
		} else {
			this.maxSel = maxSel;
		}
	}

	/**
	 * getter method
	 * @return the itemTag
	 */

	public String[] getItemTag() {
		return itemTag;
	}

	/**
	 * setter method
	 * @param itemTag the itemTag to set
	 */

	public void setItemTag(String[] itemTag) {
		this.itemTag = itemTag;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getLimitCounts() {
		return limitCounts;
	}

	public void setLimitCounts(Integer limitCounts) {
		this.limitCounts = limitCounts;
	}

	public Integer getLeftCounts() {
		return leftCounts;
	}

	public void setLeftCounts(Integer leftCounts) {
		this.leftCounts = leftCounts;
	}

	public Integer getSpicy() {
		return spicy;
	}

	public void setSpicy(Integer spicy) {
		this.spicy = spicy;
	}

	public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}

	public Integer getDishType() {
		return dishType;
	}

	public void setDishType(Integer dishType) {
		this.dishType = dishType;
	}

	
	
}
