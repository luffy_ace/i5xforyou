package com.wuuxiang.i5xforyou.ilook.vo;

import org.apache.commons.lang.StringUtils;

public class ItemMulitUnit {
	// piu.itemid, piu.unid, piu.unname, piu.unpr, piu.hypr, piu.isdefult
	private String mcId; // 门店ID
	private String itemId; // 菜品ID
	private String unId; // 单位ID
	private String unName; // 单位名换
	private String unPrice; // 标准价
	
	private String hyPrice; // 会员价
	private String isDefault; // 默认选中
	
	private Integer isSale = 1;//分时段是否售卖，1售卖，0不售卖
    private String promPrice;//促销价
	private String promBeginTime;//促销开始时间
	private String promEndTime;//促销结束时间

	private Integer limitCounts; //库存限量
	private Integer leftCounts;//剩余数量
	
	public Integer getIsSale() {
		return isSale;
	}

	public void setIsSale(Integer isSale) {
		this.isSale = isSale;
	}

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getUnId() {
		return unId;
	}

	public void setUnId(String unId) {
		this.unId = unId;
	}

	public String getUnName() {
		return unName;
	}

	public void setUnName(String unName) {
		this.unName = unName;
	}

	public String getUnPrice() {
		return unPrice;
	}

	public void setUnPrice(String unPrice) {
		this.unPrice = unPrice;
	}

	public String getHyPrice() {
		return hyPrice;
	}

	public void setHyPrice(String hyPrice) {
		this.hyPrice = hyPrice;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getPromPrice() {
		return promPrice;
	}

	public void setPromPrice(String promPrice) {
		this.promPrice = promPrice;
	}

	public String getPromBeginTime() {
		if(StringUtils.isNotBlank(promBeginTime)&&promBeginTime.length()>5){
			promBeginTime=promBeginTime.substring(0,5);
		}
		return promBeginTime;
	}

	public void setPromBeginTime(String promBeginTime) {
		this.promBeginTime = promBeginTime;
	}

	public String getPromEndTime() {
		if(StringUtils.isNotBlank(promEndTime)&&promEndTime.length()>5){
			promEndTime=promEndTime.substring(0, 5);
		}
		return promEndTime;
	}

	public void setPromEndTime(String promEndTime) {
		this.promEndTime = promEndTime;
	}

	public Integer getLimitCounts() {
		return limitCounts;
	}

	public void setLimitCounts(Integer limitCounts) {
		this.limitCounts = limitCounts;
	}

	public Integer getLeftCounts() {
		return leftCounts;
	}

	public void setLeftCounts(Integer leftCounts) {
		this.leftCounts = leftCounts;
	}

	@Override
	public String toString() {
		return "ItemMulitUnit [mcId=" + mcId + ", itemId=" + itemId + ", unId=" + unId + ", unName=" + unName
				+ ", unPrice=" + unPrice + ", promPrice=" + promPrice + ", promBeginTime=" + promBeginTime
				+ ", promEndTime=" + promEndTime + ", hyPrice=" + hyPrice + ", isDefault=" + isDefault + "]";
	}

	
}
