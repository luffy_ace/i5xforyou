package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.ArrayList;
import java.util.List;

public class TcGroup {
	// 套餐分组云端ID
	private Integer gId;
	// 套餐云端ID (关联TcItem.id)
	private Integer tId;
	// 分组ID
	private Integer grpId;
	// 商家ID
	private Integer mcId;
	// 套餐ID
	private Integer tcId;
	// 分组名称
	private String grpName;
	// 分组编号
	private String grpCode;
	// 品项可选最大数量
	private Integer maxSelCount;
	// 品项可选最小数量
	private Integer minSelCount;
	// 分组菜品数据
	private List<TcGroupItem> tcGroupItems = new ArrayList<TcGroupItem>(0);

	public Integer getgId() {
		return gId;
	}

	public void setgId(Integer gId) {
		this.gId = gId;
	}

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public Integer getGrpId() {
		return grpId;
	}

	public void setGrpId(Integer grpId) {
		this.grpId = grpId;
	}

	public Integer getMcId() {
		return mcId;
	}

	public void setMcId(Integer mcId) {
		this.mcId = mcId;
	}

	public Integer getTcId() {
		return tcId;
	}

	public void setTcId(Integer tcId) {
		this.tcId = tcId;
	}

	public String getGrpName() {
		return grpName;
	}

	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}

	public String getGrpCode() {
		return grpCode;
	}

	public void setGrpCode(String grpCode) {
		this.grpCode = grpCode;
	}

	public Integer getMaxSelCount() {
		return maxSelCount;
	}

	public void setMaxSelCount(Integer maxSelCount) {
		this.maxSelCount = maxSelCount;
	}

	public Integer getMinSelCount() {
		return minSelCount;
	}

	public void setMinSelCount(Integer minSelCount) {
		this.minSelCount = minSelCount;
	}

	public List<TcGroupItem> getTcGroupItems() {
		return tcGroupItems;
	}

	public void setTcGroupItems(List<TcGroupItem> tcGroupItems) {
		this.tcGroupItems = tcGroupItems;
	}

	@Override
	public String toString() {
		return "TcGroup [grpId=" + grpId + ", mcId=" + mcId + ", tcId=" + tcId + ", grpName=" + grpName + ", grpCode="
				+ grpCode + ", maxSelCount=" + maxSelCount + ", minSelCount=" + minSelCount + ", tcGroupItems="
				+ tcGroupItems + "]";
	}
}
