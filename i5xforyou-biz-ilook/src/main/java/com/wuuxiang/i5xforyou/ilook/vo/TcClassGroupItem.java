package com.wuuxiang.i5xforyou.ilook.vo;

public class TcClassGroupItem {

	private TcClass tcClass;
	private TcItem tcItem;
	private TcGroup tcGroup;
	private TcGroupItem tcGroupItem;

	public TcClass getTcClass() {
		return tcClass;
	}

	public void setTcClass(TcClass tcClass) {
		this.tcClass = tcClass;
	}

	public TcItem getTcItem() {
		return tcItem;
	}

	public void setTcItem(TcItem tcItem) {
		this.tcItem = tcItem;
	}

	public TcGroup getTcGroup() {
		return tcGroup;
	}

	public void setTcGroup(TcGroup tcGroup) {
		this.tcGroup = tcGroup;
	}

	public TcGroupItem getTcGroupItem() {
		return tcGroupItem;
	}

	public void setTcGroupItem(TcGroupItem tcGroupItem) {
		this.tcGroupItem = tcGroupItem;
	}

}
