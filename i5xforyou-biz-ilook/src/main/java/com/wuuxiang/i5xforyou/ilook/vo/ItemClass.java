package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.ArrayList;
import java.util.List;

public class ItemClass {

	private String itemClassId; // 菜品小类
	private String name; // 小类名称
	private List<Item> items = new ArrayList<Item>(0);  //菜品数据

	public String getItemClassId() {
		return itemClassId;
	}

	public void setItemClassId(String itemClassId) {
		this.itemClassId = itemClassId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}



}
