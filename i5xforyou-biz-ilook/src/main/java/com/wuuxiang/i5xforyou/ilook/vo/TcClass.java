package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.ArrayList;
import java.util.List;

public class TcClass {
	// 套餐类别ID
	private Integer tcClassId;
	// 商家ID
	private Integer mcId;
	// 类别名称
	private String name;
	// 类别编号
	private String code;
	// 上级类别
	private Integer pTcClassId;
	// 菜品级别
	private Integer tLevel;
	// 手工排序号
	private Integer orderNo;
	// 是否参与满立减
	private Integer mljFlg;
	// 套餐数据
	private List<TcItem> tcItems = new ArrayList<TcItem>(0);

	public Integer getTcClassId() {
		return tcClassId;
	}

	public void setTcClassId(Integer tcClassId) {
		this.tcClassId = tcClassId;
	}

	public Integer getMcId() {
		return mcId;
	}

	public void setMcId(Integer mcId) {
		this.mcId = mcId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getPreTcClassId() {
		return pTcClassId;
	}

	public void setPreTcClassId(Integer preTcClassId) {
		this.pTcClassId = preTcClassId;
	}

	public Integer getpTcClassId() {
		return pTcClassId;
	}

	public void setpTcClassId(Integer pTcClassId) {
		this.pTcClassId = pTcClassId;
	}

	public Integer gettLevel() {
		return tLevel;
	}

	public void settLevel(Integer tLevel) {
		this.tLevel = tLevel;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getMljFlg() {
		return mljFlg;
	}

	public void setMljFlg(Integer mljFlg) {
		this.mljFlg = mljFlg;
	}

	public List<TcItem> getTcItems() {
		return tcItems;
	}

	public void setTcItems(List<TcItem> tcItems) {
		this.tcItems = tcItems;
	}

	@Override
	public String toString() {
		return "TcClass [tcClassId=" + tcClassId + ", mcId=" + mcId + ", name=" + name + ", code=" + code
				+ ", pTcClassId=" + pTcClassId + ", tLevel=" + tLevel + ", orderNo=" + orderNo + ", mljFlg=" + mljFlg
				+ ", tcItems=" + tcItems + "]";
	}
}
