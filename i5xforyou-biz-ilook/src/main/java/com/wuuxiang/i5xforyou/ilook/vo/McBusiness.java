package com.wuuxiang.i5xforyou.ilook.vo;

public class McBusiness {
	// 自取模式下单
	private String dinerget;
	// 食客扫码下单
	private String ordersweepcode;
	// 桌位号下单
	private String orderinputcode;
	// 下单提示语
	private String noticedesp;
	// APP扫码下单
	private String scancodeflg;
	// 线上线下支付(1:线上先支付，0：线下后支付）
	private String orderpayflg;
	// 点餐公告
	private String ordernotice;
	// 点餐人数必输开关 0：不必输 1：必输  默认0
	private String isdinnum;
	// 享付功能开关 1开 0 关
	private Integer xfPayFlg;
	// 整单备注是否显示的控制 整单备注是否显示的控制
	private Integer ordRemFlg;
	// 新版点菜开关 1新版点菜 0 旧版点菜
	private Integer newOrderFlg;
	// 旧版点餐标识（0：不是旧版点餐；1是旧版点餐）
	private Integer oldOrderFlg;
	// 排队标识（0：没有开通排队业务；1是开通排队业务）
	private Integer queueUpFlg;
	// 在线付支付标识。0：未开通；1：开通。默认0
	private Integer zxfPayFlg;
	// 会员认证提示标识。0：未开通；1：开通。默认1
	private Integer zxfPayMemFlg;
	// 在线付拉取订单实时上传标记：1实时 0不实时 默认0
	private Integer zxfPayRtFlg;
	// 食客自提开关 1开 0 关
	private Integer ztFlg;

	public Integer getZxfPayRtFlg() {
		return zxfPayRtFlg;
	}

	public void setZxfPayRtFlg(Integer zxfPayRtFlg) {
		this.zxfPayRtFlg = zxfPayRtFlg;
	}

	public Integer getXfPayFlg() {
		return xfPayFlg;
	}

	public void setXfPayFlg(Integer xfPayFlg) {
		this.xfPayFlg = xfPayFlg;
	}

	public Integer getOrdRemFlg() {
		return ordRemFlg;
	}

	public void setOrdRemFlg(Integer ordRemFlg) {
		this.ordRemFlg = ordRemFlg;
	}

	public String getNoticedesp() {
		return noticedesp;
	}

	public void setNoticedesp(String noticedesp) {
		this.noticedesp = noticedesp;
	}

	public String getOrderpayflg() {
		return orderpayflg;
	}

	public void setOrderpayflg(String orderpayflg) {
		this.orderpayflg = orderpayflg;
	}

	public String getDinerget() {
		return dinerget;
	}

	public void setDinerget(String dinerget) {
		this.dinerget = dinerget;
	}

	public String getOrdersweepcode() {
		return ordersweepcode;
	}

	public void setOrdersweepcode(String ordersweepcode) {
		this.ordersweepcode = ordersweepcode;
	}

	public String getOrderinputcode() {
		return orderinputcode;
	}

	public void setOrderinputcode(String orderinputcode) {
		this.orderinputcode = orderinputcode;
	}

	public String getOrdernotice() {
		return ordernotice;
	}

	public void setOrdernotice(String ordernotice) {
		this.ordernotice = ordernotice;
	}

	public String getScancodeflg() {
		return scancodeflg;
	}

	public void setScancodeflg(String scancodeflg) {
		this.scancodeflg = scancodeflg;
	}

	public String getIsdinnum() {
		return isdinnum;
	}

	public void setIsdinnum(String isdinnum) {
		this.isdinnum = isdinnum;
	}

	public Integer getNewOrderFlg() {
		return newOrderFlg;
	}

	public void setNewOrderFlg(Integer newOrderFlg) {
		this.newOrderFlg = newOrderFlg;
	}

	public Integer getZxfPayFlg() {
		return zxfPayFlg;
	}

	public void setZxfPayFlg(Integer zxfPayFlg) {
		this.zxfPayFlg = zxfPayFlg;
	}

	public Integer getOldOrderFlg() {
		return oldOrderFlg;
	}

	public void setOldOrderFlg(Integer oldOrderFlg) {
		this.oldOrderFlg = oldOrderFlg;
	}

	public Integer getQueueUpFlg() {
		return queueUpFlg;
	}

	public void setQueueUpFlg(Integer queueUpFlg) {
		this.queueUpFlg = queueUpFlg;
	}


	public Integer getZxfPayMemFlg() {
		return zxfPayMemFlg;
	}

	public void setZxfPayMemFlg(Integer zxfPayMemFlg) {
		this.zxfPayMemFlg = zxfPayMemFlg;
	}

	public Integer getZtFlg() {
		return ztFlg;
	}

	public void setZtFlg(Integer ztFlg) {
		this.ztFlg = ztFlg;
	}

	@Override
	public String toString() {
		return "McBusiness [dinerget=" + dinerget + ", ordersweepcode=" + ordersweepcode + ", orderinputcode="
				+ orderinputcode + ", noticedesp=" + noticedesp + ", scancodeflg=" + scancodeflg + ", orderpayflg="
				+ orderpayflg + ", ordernotice=" + ordernotice + ", isdinnum=" + isdinnum + ", xfPayFlg=" + xfPayFlg
				+ ", ordRemFlg=" + ordRemFlg + ", newOrderFlg=" + newOrderFlg + ", oldOrderFlg=" + oldOrderFlg
				+ ", queueUpFlg=" + queueUpFlg + ", zxfPayFlg=" + zxfPayFlg + ", zxfPayMemFlg=" + zxfPayMemFlg
				+ ", zxfPayRtFlg=" + zxfPayRtFlg + ", ztFlg=" + ztFlg + "]";
	}

}
