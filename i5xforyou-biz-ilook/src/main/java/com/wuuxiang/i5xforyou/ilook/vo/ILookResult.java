package com.wuuxiang.i5xforyou.ilook.vo;

/** 
 * @ClassName: ILookResult
 * @Description: 接口共同返回结果
 * @author Mobile Web Group-lff
 * @date 2018年4月26日 上午11:39:56
 *
 */
public class ILookResult {
	public static final String DB_SELECT = "DB数据库查询";
	public static final String REDIS_SELECT = "REDIS缓存查询";
	private String api;  //接口名称
	private String params;  //接口入参
	private String rst;  //接口出参
	private Object value;//真正需要的出参
	
	
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getRst() {
		return rst;
	}
	public void setRst(String rst) {
		this.rst = rst;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
}
