package com.wuuxiang.i5xforyou.ilook.service;

import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;

/** 
 * @ClassName: DuoRenDianCaiService
 * @Description: 多人点餐
 * @author Mobile Web Group-lff
 * @date 2018年1月23日 上午9:32:05
 *
 */

public interface DuoRenDianCaiService {
	
	/**
	 * getBeforeSubmitFlg
	 * @Description: 获取提前下单标志
	 * @author Mobile Web Group-lff
	 * @date 2018年4月27日 下午12:36:42
	 *
	 * @param mcId
	 * @param tableNo
	 * @param openId
	 * @return
	 * @return ILookResult
	 */
	public ILookResult getBeforeSubmitFlg(String mcId, String tableNo, String openId);
	
	
	
}
