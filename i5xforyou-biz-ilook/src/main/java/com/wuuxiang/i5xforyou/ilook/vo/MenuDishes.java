package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.ArrayList;
import java.util.List;

/** 
 * @ClassName: MenuDishes
 * @Description: 菜谱数据
 * @author Mobile Web Group-lff
 * @date 2018年4月10日 下午5:26:50
 *
 */

public class MenuDishes {
	
	//top和推荐菜数据，包含必选餐具数据，一般size=3
	private List<MenuGroup> topAndTjData = new ArrayList<MenuGroup>();
	//提前下单菜品数据
	private MenuGroup firstSumbitDish;
	
	/**
	 * getEmptyMenuGroup
	 * @Description: 获取一个空的提前下单数据结构，给预点，自提用
	 * @author Mobile Web Group-lff
	 * @date 2018年4月12日 下午2:41:23
	 *
	 * @return MenuGroup
	 */
	public MenuGroup getEmptyFirstSumbitDish() {
		return new MenuGroup();
	}
	
	public List<MenuGroup> getTopAndTjData() {
		return topAndTjData;
	}
	public void setTopAndTjData(List<MenuGroup> topAndTjData) {
		this.topAndTjData = topAndTjData;
	}
	public MenuGroup getFirstSumbitDish() {
		return firstSumbitDish;
	}
	public void setFirstSumbitDish(MenuGroup firstSumbitDish) {
		this.firstSumbitDish = firstSumbitDish;
	}
	
}
