package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wuuxiang.i5xforyou.ilook.util.ValidatorUtil;

/**
 * @author Yong
 *
 */
public class TcItem {
	// 套餐云端ID
	private Integer tId;
	// 套餐code
    private String code;
	// 套餐ID
	private Integer tcId;
	// 商家ID
	private Integer mcId;
	// 套餐类别ID
	private Integer tcClassId;
	// 套餐名称
	private String name;
	// 套餐单位
	private String unitName;
	// 套餐价格
	private String tcPrice;
	// 缩略图文件名
	private String taFileName;
	// 沽清标记
	private Integer gqFlg;
	// 分时段售卖标记，1售卖，0不售卖
	private Integer isSale = 1;
	private String hyjFlg = "0";
	private String  vipPrice;
	// 套餐分组数据
	private List<TcGroup> tcGroups = new ArrayList<TcGroup>(0);
	
	private String boxPrice; //餐盒费
	
	private String[] itemTag;
	
	private String desp; // 描述

	private Integer limitCounts; //库存限量
	private Integer leftCounts;//剩余数量
	private Integer spicy; //菜品辣度
	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getGqFlg() {
		return gqFlg;
	}

	public void setGqFlg(Integer gqFlg) {
		this.gqFlg = gqFlg;
	}

	public Integer getTcId() {
		return tcId;
	}

	public void setTcId(Integer tcId) {
		this.tcId = tcId;
	}

	public Integer getMcId() {
		return mcId;
	}

	public void setMcId(Integer mcId) {
		this.mcId = mcId;
	}

	public Integer getTcClassId() {
		return tcClassId;
	}

	public void setTcClassId(Integer tcClassId) {
		this.tcClassId = tcClassId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getTcPrice() {
		return tcPrice;
	}

	public void setTcPrice(String tcPrice) {
		this.tcPrice = tcPrice;
	}

	public String getTaFileName() {
		if(StringUtils.isBlank(taFileName)||!ValidatorUtil.isUrl(taFileName)) {
			return "//cdnpic.wuuxiang.com/pic/merchant/nopic/nopic.jpg";
		}
		return taFileName;
	}

	public void setTaFileName(String taFileName) {
		this.taFileName = taFileName;
	}

	public List<TcGroup> getTcGroups() {
		return tcGroups;
	}

	public void setTcGroups(List<TcGroup> tcGroups) {
		this.tcGroups = tcGroups;
	}

	public String getBoxPrice() {
		return boxPrice;
	}

	public void setBoxPrice(String boxPrice) {
		this.boxPrice = boxPrice;
	}

	/**
	 * getter method
	 * @return the itemTag
	 */
	
	public String[] getItemTag() {
		return itemTag;
	}

	/**
	 * setter method
	 * @param itemTag the itemTag to set
	 */
	
	public void setItemTag(String[] itemTag) {
		this.itemTag = itemTag;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	public Integer getIsSale() {
		return isSale;
	}

	public void setIsSale(Integer isSale) {
		this.isSale = isSale;
	}

	public Integer getLimitCounts() {
		return limitCounts;
	}

	public void setLimitCounts(Integer limitCounts) {
		this.limitCounts = limitCounts;
	}

	public Integer getLeftCounts() {
		return leftCounts;
	}

	public void setLeftCounts(Integer leftCounts) {
		this.leftCounts = leftCounts;
	}

	public Integer getSpicy() {
		return spicy;
	}

	public void setSpicy(Integer spicy) {
		this.spicy = spicy;
	}
	public String getHyjFlg() {
		return hyjFlg;
	}

	public void setHyjFlg(String hyjFlg) {
		this.hyjFlg = hyjFlg;
	}

	public String getVipPrice() {
		return vipPrice;
	}

	public void setVipPrice(String vipPrice) {
		this.vipPrice = vipPrice;
	}

	@Override
	public String toString() {
		return "TcItem [tcId=" + tcId + ", mcId=" + mcId + ", tcClassId=" + tcClassId + ", name=" + name + ", unitName="
				+ unitName + ", tcPrice=" + tcPrice + ", taFileName=" + taFileName + ", gqFlg=" + gqFlg + ", tcGroups="
				+ tcGroups + ", desp=" + desp + ", spicy=" + spicy +"]";
	}
}
