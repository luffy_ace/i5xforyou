package com.wuuxiang.i5xforyou.ilook.vo;

public class TcGroupItem {
	// 套餐类别ID
	private Integer tcClassId;
	// 套餐ID
	private Integer tcId;
	// 套餐分组菜品云端ID
	private Integer id;
	// 套餐分组云端ID(关联TcGroup.id)
	private Integer gId;
	// 分组ID
	private Integer grpId;
	// 商家ID
	private Integer mcId;
	// 菜品ID
	private Integer itemId;
	// 菜品名称
	private String name;
	// 菜品单位ID
	private Integer unid;
	// 菜品单位
	private String unitName;
	// 菜品价格
	private String itemPrice;
	// 菜品加价
	private String addPrice;
	// 菜品编号
	private String code;
	// 品项限定数量
	private Integer maxCount;
	// 品项默认数量
	private Integer defaultCount;
	// 是否必选，0：不必选；1：必选
	private Integer mustSelFlg;
	// 描述
	private String desp;
	// 沽清标记
	private Integer gqFlg;


	public Integer getTcClassId() {
		return tcClassId;
	}

	public void setTcClassId(Integer tcClassId) {
		this.tcClassId = tcClassId;
	}

	public Integer getTcId() {
		return tcId;
	}

	public void setTcId(Integer tcId) {
		this.tcId = tcId;
	}

	public Integer getUnid() {
		return unid;
	}

	public void setUnid(Integer unid) {
		this.unid = unid;
	}

	public Integer getgId() {
		return gId;
	}

	public void setgId(Integer gId) {
		this.gId = gId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGqFlg() {
		return gqFlg;
	}

	public void setGqFlg(Integer gqFlg) {
		this.gqFlg = gqFlg;
	}

	public Integer getGrpId() {
		return grpId;
	}

	public void setGrpId(Integer grpId) {
		this.grpId = grpId;
	}

	public Integer getMcId() {
		return mcId;
	}

	public void setMcId(Integer mcId) {
		this.mcId = mcId;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getAddPrice() {
		return addPrice;
	}

	public void setAddPrice(String addPrice) {
		this.addPrice = addPrice;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public Integer getDefaultCount() {
		return defaultCount;
	}

	public void setDefaultCount(Integer defaultCount) {
		this.defaultCount = defaultCount;
	}

	public Integer getMustSelFlg() {
		return mustSelFlg;
	}

	public void setMustSelFlg(Integer mustSelFlg) {
		this.mustSelFlg = mustSelFlg;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	@Override
	public String toString() {
		return "TcGroupItem [id=" + id + ", gId=" + gId + ", grpId=" + grpId + ", mcId=" + mcId + ", itemId=" + itemId
				+ ", name=" + name + ", unid=" + unid + ", unitName=" + unitName + ", itemPrice=" + itemPrice
				+ ", addPrice=" + addPrice + ", code=" + code + ", maxCount=" + maxCount + ", defaultCount="
				+ defaultCount + ", mustSelFlg=" + mustSelFlg + ", desp=" + desp + ", gqFlg=" + gqFlg + "]";
	}


}
