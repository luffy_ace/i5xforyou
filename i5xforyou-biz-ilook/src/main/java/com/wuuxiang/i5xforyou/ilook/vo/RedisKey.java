package com.wuuxiang.i5xforyou.ilook.vo;

public class RedisKey {

	// 下划线连接符
	public static final String UNDERLINE = "_";
	// 减号连接符
	public static final String MINUS = "-";
	// 桌号点餐标识
	public static final String TABLE = "T";
	// 单人点餐标识
	public static final String PEOPLE = "P";

	// 微信接口门店对应关系KEY
    public static final String REDIS_WECHAT_APIMC = "i5xwxplus.rediscache_wechat_apimc";
    // 支付宝接口门店对应关系KEY
    public static final String REDIS_ALIPAY_APIMC = "i5xwxplus.rediscache_alipay_apimc";
    // 点餐人数
    public static final String REDIS_PEOPLE_COUNT = "i5xwxplus.people_count";
    // 门店模版
    public static final String REDIS_MCTEMPLATENAME = "i5xwxplus.mc_template_name";
    // 门店名称
    public static final String REDIS_MCNAME = "i5xwxplus.mc_name";
    // 点餐即支付订单缓存锁
    public static final String REDIS_PHO_ORDER_PAY_LOCK = "i5xwxplus.pho_order_pay_lock";
    // 点餐即支付订单缓存锁的过期时间 单位second
    public static final int REDIS_PHO_ORDER_PAY_LOCK_EX = 5*60;//5分钟
    // 用户地理位置
    public static final String REDIS_USERLOCATION = "i5xwxplus.userlocation";
    // 预定单前缀
    public static final String PRE_REIDS_YU_DIAN_CAI_CART = "i5xwxpro.yudiancai_cart_";
    // CRM版本号
    public static final String REDIS_CRMVERSION = "i5xwxplus.crmversion";
    // 会员卡权益 gcid
    public static final String REDIS_USER_PRIVILEGE = "i5xwxplus.user_privilege";
    // 桌位名称
    public static final String REDIS_TABLE_NAME = "i5xwxplus.table_name";
    // 短信发送时间戳
    public static final String REDIS_SMSSENDTIME = "i5xwxplus.sms.sendtime";
    // 支付宝口碑流水号对应的人
    public static final String REDIS_KOUBEI_SERIALNO_UID = "i5xwxplus_koubei_serialno_uid";
    // 支付宝口碑流水号对应的人
    public static final String REDIS_KOUBEI_MCID_TABLE_UIDS = "i5xwxplus_koubei_mcid_table_uids";
    // 提前下单缓存flg
    public static final String REDIS_FIRST_SUMBIT_DISH_FLG = "i5xwxplus.first_sumbit_dish_flg";

    
    // 云端绑定openid
    public static final String REDIS_O2OBDOPENID = "o2o-bdopenid";
    // 云端绑定平台的 openid
    public static final String REDIS_O2OMPBDOPENID = "o2o-mp-bdopenid";
    // 云端绑定unionid
    public static final String REDIS_O2OBDUNIONID = "o2o-bdunionid";
    // 云端分时段菜品前缀
    public static final String REDIS_O2OCOMMMARKETPLAN = "o2o-comm-marketplan";

    // 门店菜品更新时间
    public static final String REIDS_MC_ITEMS_UPDATE_TIME = "i5xwxplus.mc_items_update_time";

    // 支付宝128加密的appId key
    public static final String REIDS_O2O_PAY_TBAPPID_NOTUPGRADE = "o2o-pay-tbappid-notupgrade";
    // 门店mcbusiness开关控制
    public static final String REDIS_MCBUSINESS = "i5xwxplus.mcbusiness";

}