package com.wuuxiang.i5xforyou.ilook.service;

import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;

/** 
 * @ClassName: ShopService
 * @Description: 门店
 * @author Mobile Web Group-lff
 * @date 2018年4月26日 上午11:31:28
 *
 */

public interface ShopService {
	
	/**
	 * getMcTemplate
	 * @Description: 获取门店模板
	 * @author Mobile Web Group-lff
	 * @date 2018年4月27日 上午8:50:29
	 *
	 * @param mcId
	 * @return
	 * @return ILookResult
	 */
	public ILookResult getMcTemplate(String mcId);
	
	/**
	 * getSerialNo
	 * @Description: 获取流水号
	 * @author Mobile Web Group-lff
	 * @date 2018年4月27日 上午8:50:40
	 *
	 * @param mcId
	 * @param tableNo
	 * @param openId
	 * @return
	 * @return ILookResult
	 */
	public ILookResult getSerialNo(String mcId, String tableNo, String openId);

	/**
	 * getPho_mc
	 * @Description: 获取门店业务
	 * @author Mobile Web Group-lff
	 * @date 2018年4月27日 上午8:50:29
	 *
	 * @param mcId
	 * @return
	 * @return ILookResult
	 */
	public ILookResult getPho_mc(String mcId);
	
	/**
	 * getPho_mcbusiness
	 * @Description: 获取门店业务
	 * @author Mobile Web Group-lff
	 * @date 2018年4月27日 上午8:50:29
	 *
	 * @param mcId
	 * @return
	 * @return ILookResult
	 */
	public ILookResult getPho_mcbusiness(String mcId);
	
	/**
	 * getPho_menu_group <br/>
	 * 获取菜谱设置 <br/>
	 * 
	 * @date 2018年1月23日 上午10:31:49
	 *
	 * @param mcId
	 * @return ILookResult
	 */
	public ILookResult getPho_menu_group(String mcId);
	
	
	
}
