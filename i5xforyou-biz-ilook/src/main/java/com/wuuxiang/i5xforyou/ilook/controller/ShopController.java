package com.wuuxiang.i5xforyou.ilook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wuuxiang.i5xforyou.common.controller.BaseController;
import com.wuuxiang.i5xforyou.ilook.service.ShopService;
import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;

/** 
 * @ClassName: ShopController
 * @Description: 门店相关信息
 * @author Mobile Web Group-lff
 * @date 2018年4月26日 上午11:29:03
 *
 */
@RestController
public class ShopController extends BaseController {
	
	@Autowired
	private ShopService shopService;
	
	//获取门店Template
	@RequestMapping(value="/shop/mcTemplate", method=RequestMethod.GET) 
	public ILookResult getMcTemplate(String mcId) {
		
		return shopService.getMcTemplate(mcId);
	}
	
	//流水号
	@RequestMapping(value="/shop/serialNo", method=RequestMethod.GET) 
	public ILookResult getSerialNo(String mcId, String tableNo, String openId) {
		
		return shopService.getSerialNo(mcId, tableNo, openId);
	}

	//PHO_MC
	@RequestMapping(value="/shop/phomc", method=RequestMethod.GET) 
	public ILookResult getPho_mc(String mcId) {
		
		return shopService.getPho_mc(mcId);
	}
	
	//PHO_MCBUSINESS
	@RequestMapping(value="/shop/phomcbusiness", method=RequestMethod.GET) 
	public ILookResult getPho_mcbusiness(String mcId) {
		
		return shopService.getPho_mcbusiness(mcId);
	}
	
	//获取菜谱设置
	@RequestMapping(value="/shop/phomenugroup", method=RequestMethod.GET) 
	public ILookResult getPho_menu_group(String mcId) {
		
		return shopService.getPho_menu_group(mcId);
	}
	
}
