package com.wuuxiang.i5xforyou.ilook.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.wuuxiang.i5xforyou.common.service.impl.BaseServiceImpl;
import com.wuuxiang.i5xforyou.ilook.service.DuoRenDianCaiService;
import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;
import com.wuuxiang.i5xforyou.ilook.vo.RedisKey;

/** 
 * @ClassName: DuoRenDianCaiServiceImpl
 * @Description: 多人点餐
 * @author Mobile Web Group-lff
 * @date 2018年1月23日 上午9:32:33
 *
 */
@Service("duoRenDianCaiService")
public class DuoRenDianCaiServiceImpl extends BaseServiceImpl implements DuoRenDianCaiService {

	@Autowired
	private StringRedisTemplate redisTemplate;
	
	/**
	 * Description: 获取提前下单标志
	 * @author: Mobile Web Group-lff
	 * @date: 2018年4月27日 下午12:36:56
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.DuoRenDianCaiService#getBeforeSubmitFlg(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ILookResult getBeforeSubmitFlg(String mcId, String tableNo, String openId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		
		//"1":有提交过提前下单，"0"：没有提交过提前下单
		String redisKey = getFirstSumbitDishFlgRedisKey(mcId, tableNo, openId);
		String firstSumbitDishFlg = redisTemplate.opsForValue().get(redisKey);
		if (StringUtils.isBlank(firstSumbitDishFlg)) {
			firstSumbitDishFlg = "0";
		}
		
		iLookResult.setApi(ILookResult.REDIS_SELECT);
		iLookResult.setParams(redisKey);
		iLookResult.setRst(firstSumbitDishFlg);
		iLookResult.setValue(firstSumbitDishFlg);
		return iLookResult;
	}

	//提前提交下单redis key
	private String getFirstSumbitDishFlgRedisKey(String mcId, String tableNo, String openId){
		StringBuffer redisKey = new StringBuffer(RedisKey.REDIS_FIRST_SUMBIT_DISH_FLG);
		redisKey.append(RedisKey.UNDERLINE);
		redisKey.append(mcId);
		redisKey.append(RedisKey.UNDERLINE);
		if (StringUtils.isNotBlank(tableNo)) {
			redisKey.append(tableNo);
		} else {
			redisKey.append(openId);
		}
		return redisKey.toString();
	}

	
}
