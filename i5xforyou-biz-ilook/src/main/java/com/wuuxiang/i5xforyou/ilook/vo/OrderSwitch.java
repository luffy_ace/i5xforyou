package com.wuuxiang.i5xforyou.ilook.vo;

public class OrderSwitch {

	//是否开启外送
	private Integer deliveryFlg;
	
	//是否开启线上点餐
	private Integer orderFlg;
	
	//是否开启导航页的充值
	private Integer smczFlg;

	/**
	 * getter method
	 * @return the deliveryFlg
	 */
	
	public Integer getDeliveryFlg() {
		return deliveryFlg;
	}

	/**
	 * setter method
	 * @param deliveryFlg the deliveryFlg to set
	 */
	
	public void setDeliveryFlg(Integer deliveryFlg) {
		this.deliveryFlg = deliveryFlg;
	}

	/**
	 * getter method
	 * @return the orderFlg
	 */
	
	public Integer getOrderFlg() {
		return orderFlg;
	}

	/**
	 * setter method
	 * @param orderFlg the orderFlg to set
	 */
	
	public void setOrderFlg(Integer orderFlg) {
		this.orderFlg = orderFlg;
	}

	/**
	 * getter method
	 * @return the smczFlg
	 */
	
	public Integer getSmczFlg() {
		return smczFlg;
	}

	/**
	 * setter method
	 * @param smczFlg the smczFlg to set
	 */
	
	public void setSmczFlg(Integer smczFlg) {
		this.smczFlg = smczFlg;
	}
	
	
}
