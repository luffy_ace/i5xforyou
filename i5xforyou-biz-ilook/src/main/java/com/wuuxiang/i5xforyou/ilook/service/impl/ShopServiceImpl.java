package com.wuuxiang.i5xforyou.ilook.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wuuxiang.i5xforyou.common.consts.Consts;
import com.wuuxiang.i5xforyou.common.service.impl.BaseServiceImpl;
import com.wuuxiang.i5xforyou.ilook.service.ShopService;
import com.wuuxiang.i5xforyou.ilook.vo.ILookResult;
import com.wuuxiang.i5xforyou.ilook.vo.McBusiness;
import com.wuuxiang.i5xforyou.ilook.vo.MenuGroup;
import com.wuuxiang.i5xforyou.ilook.vo.OrderSwitch;
import com.wuuxiang.i5xforyou.ilook.vo.RedisKey;
import com.wuuxiang.i5xforyou.utils.HttpUtil;
import com.wuuxiang.jdbc.spring.boot.autoconfigure.dao.O2OJdbcRepository;

/** 
 * @ClassName: ShopServiceImpl
 * @Description: 门店
 * @author Mobile Web Group-lff
 * @date 2018年4月26日 下午1:20:28
 *
 */
@Service("shopService")
public class ShopServiceImpl extends BaseServiceImpl implements ShopService {
	
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	@Autowired
	private O2OJdbcRepository<String> o2oString;
	
	@Autowired
	private O2OJdbcRepository<McBusiness> o2oMcBusiness;
	
	@Autowired
	private O2OJdbcRepository<MenuGroup> o2oMenuGroup;
	
	@Autowired
	private O2OJdbcRepository<OrderSwitch> o2oOrderSwitch;
	
	/**
	 * Description: 门店模板
	 * @author: Mobile Web Group-lff
	 * @date: 2018年4月26日 下午1:24:27
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.ShopService#getMcTemplate(java.lang.String)
	 */
	@Override
	public ILookResult getMcTemplate(String mcId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		
		String mcTemplate = redisTemplate.opsForValue().get(RedisKey.REDIS_MCTEMPLATENAME + "_" + mcId);
		if (StringUtils.isBlank(mcTemplate)){
			StringBuilder querySql = new StringBuilder();
			querySql.append("SELECT IDENTITYNAME FROM( SELECT PT.IDENTITYNAME AS identityname, PMM.MODIFYTIME FROM ");
			querySql.append("PHO_MENU_MCID PMM, PHO_MENU PM, PHO_TEMPLATE PT WHERE PMM.MENUID = PM. ID AND PM.TEMPLATEID = PT. ID AND PMM.MCID =  ");
			querySql.append(mcId);
			querySql.append(" ORDER BY MODIFYTIME DESC ) WHERE ROWNUM = 1 ");
			String templatename = o2oString.getJavaBean(querySql.toString(), String.class);
			mcTemplate = StringUtils.isBlank(templatename)?"templateA":templatename;
			
			iLookResult.setApi(ILookResult.DB_SELECT);
			iLookResult.setParams(querySql.toString());
			iLookResult.setRst(mcTemplate);
			iLookResult.setValue(mcTemplate);
			return iLookResult;
		}
		
		iLookResult.setApi(ILookResult.REDIS_SELECT);
		iLookResult.setParams(RedisKey.REDIS_MCTEMPLATENAME + "_" + mcId);
		iLookResult.setRst(mcTemplate);
		iLookResult.setValue(mcTemplate);
		return iLookResult;
	}
	
	/**
	 * Description: 拉流水
	 * @author: Mobile Web Group-lff
	 * @date: 2018年4月26日 下午6:20:37
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.ShopService#getSerialNo(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ILookResult getSerialNo(String mcId, String tableNo, String openId) {
		McBusiness mcBusiness = (McBusiness) this.getPho_mcbusiness(mcId).getValue();
		// 在线付是否开启线下实时上传功能 1开启0没有开启
		Integer zxfPayRtFlg = mcBusiness.getZxfPayRtFlg();
		if (zxfPayRtFlg.intValue() == 0){ // 没有开启走老的
			return this.querySerialNo(mcId, tableNo, openId);
		} else { // 开启实时上传
			return this.queryOrderRealTimeListRt(mcId, tableNo);
		}

	}
	
	//循环拉流水号
	private ILookResult querySerialNo(String mcId, String tableNo, String openId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		
		JSONObject postData = new JSONObject();
		postData.put("mcid", mcId);
		postData.put("tablenumber", tableNo);
		postData.put("openid", openId);
		Map<String, String> params= new HashMap<String, String>();
		params.put("data", postData.toString());
		String getSerialNoUrl = Consts.CLOUD_API_URL + "tcsl/GetSerialNo.htm";
		try {
			iLookResult.setApi(getSerialNoUrl);
			iLookResult.setParams(params.toString());
			JSONObject jsonObj = HttpUtil.doPostJSONObject(getSerialNoUrl, params);
			iLookResult.setRst(jsonObj.toJSONString());
			
			if (jsonObj.getIntValue("returnCode") != 1 || !"200".equals(jsonObj.getString("errorCode"))) {
				//没取到流水号
				return iLookResult;
			}
			String state = jsonObj.getString("state");
			String serialNo = jsonObj.getString("serialNo"); // serialNo = "YY00010001-20161008-0001";
			// state  0 已上传未开台，state ""  未上传  state 1已上传已开台有流水
			if (StringUtils.isBlank(mcId) || StringUtils.isBlank(state)) {
				// 未查询到流水号信息，需要重试
				return iLookResult;
			}
			if (StringUtils.equals("0", state)){
				// state 0  已上传但关台了
				return iLookResult;
			}
			//有流水号
			iLookResult.setValue(serialNo);
			return iLookResult;
		} catch (Exception e) {
			iLookResult.setRst(e.toString());
			return iLookResult;
		}
	}
	
	//实时上传拉流水号
	private ILookResult queryOrderRealTimeListRt(String mcId, String tableNo){
		//接口出参
		ILookResult iLookResult = new ILookResult();
		// 支付前提交优惠信息数据
		JSONObject postData = new JSONObject();
		postData.put("mcID", mcId);
		postData.put("tablenumber", tableNo);
		Map<String, String> params= new HashMap<String, String>();
		params.put("data", postData.toString());
		String url = Consts.CLOUD_API_URL + "order/QueryOrderListRT.htm";
		try {
			iLookResult.setApi(url);
			iLookResult.setParams(params.toString());
			JSONObject orderListJson = HttpUtil.doPostJSONObject(url, params);
			iLookResult.setRst(orderListJson.toJSONString());
			
			int errorCode = orderListJson.getIntValue("v_error_code");
			if (errorCode != 0) {
				//没取到流水号
				return iLookResult;
			}
			JSONObject orderdata = orderListJson.getJSONObject("data");
			Integer orderstate = orderdata.getIntValue("orderstate");
			if (orderstate == 0) {
				//没取到流水号
				return iLookResult;
			}
			String serialNo = orderdata.getString("serialNo");
			int isClosed = orderdata.getIntValue("isClosed"); // 1关台 0开台中
			if (isClosed == 1){ //关台
				//没取到流水号
				return iLookResult;
			} else { // 开台中
				if (StringUtils.isBlank(serialNo)) {
					//没取到流水号
					return iLookResult;
				} else {
					//有流水号
					iLookResult.setValue(serialNo);
					return iLookResult;
				}
			}
		} catch (Exception e){
			iLookResult.setRst(e.toString());
			return iLookResult;
		}
	}

	/**
	 * Description: pho_mc
	 * @author: Mobile Web Group-lff
	 * @date: 2018年5月2日 下午2:02:04
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.ShopService#getPho_mc(java.lang.String)
	 */
	@Override
	public ILookResult getPho_mc(String mcId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		
		StringBuilder querySql = new StringBuilder();
		querySql.append("SELECT PM.DELIVERYFLG AS DELIVERYFLG, PM.ORDERFLG AS ORDERFLG, NVL(PM.SMCZFLG, 0) AS SMCZFLG ");
		querySql.append("FROM PHO_MC PM ");
		querySql.append("WHERE PM.STATE = 1 AND PM.MCID = ");
		querySql.append(mcId);
		OrderSwitch orderSwitch = o2oOrderSwitch.getJavaBean(querySql.toString(), OrderSwitch.class);
		
		if (orderSwitch != null && "templateC".equals(getMcTemplate(mcId))) {
			//C模板不支持充值
			orderSwitch.setSmczFlg(0);
		}
		
		iLookResult.setApi(ILookResult.DB_SELECT);
		iLookResult.setParams(querySql.toString());
		iLookResult.setRst(JSON.toJSONString(orderSwitch));
		iLookResult.setValue(orderSwitch);
		
		return iLookResult;
	}
	
	/**
	 * Description: 获取门店业务
	 * @author: Mobile Web Group-lff
	 * @date: 2018年4月27日 上午10:38:03
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.ShopService#getPho_mcbusiness(java.lang.String)
	 */
	@Override
	public ILookResult getPho_mcbusiness(String mcId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		McBusiness mcBiz = JSON.parseObject(redisTemplate.opsForValue().get(RedisKey.REDIS_MCBUSINESS + "_" + mcId), McBusiness.class);
		if (mcBiz == null){
			StringBuilder sql = new StringBuilder(" SELECT PM.ZXFPAYMEMFLG, PM.ORDERPAYFLG, PM.SERVICESWEEPCODE AS SCANCODEFLG, PM.DINERGET, PM.ORDERSWEEPCODE, PM.ORDERINPUTCODE, PM.ORDERNOTICE, PM.NOTICEDESP,PM.ISDINNUM, PM.ORDREMFLG, NVL(PM.XFPAYFLG,0) AS XFPAYFLG, NVL(PM.NEWORDERFLG, 0) AS NEWORDERFLG, NVL(PM.ZXFPAYFLG, 0) AS ZXFPAYFLG,NVL(PM.QUEUEUPFLG, 0) AS QUEUEUPFLG, NVL(PM.OLDORDERFLG, 0) AS OLDORDERFLG, NVL(PM.ZTFLG, 0) AS ZTFLG, NVL(PM.ZXFPAYRTFLG, 0) AS ZXFPAYRTFLG FROM PHO_MCBUSINESS PM WHERE 1 = 1 AND  PM.MCID = ");
			sql.append(mcId);
			mcBiz = o2oMcBusiness.getJavaBean(sql.toString(), McBusiness.class);
			
			iLookResult.setApi(ILookResult.DB_SELECT);
			iLookResult.setParams(sql.toString());
			iLookResult.setRst(JSON.toJSONString(mcBiz));
			iLookResult.setValue(mcBiz);
			return iLookResult;
		}
		iLookResult.setApi(ILookResult.REDIS_SELECT);
		iLookResult.setParams(RedisKey.REDIS_MCBUSINESS + "_" + mcId);
		iLookResult.setRst(JSON.toJSONString(mcBiz));
		iLookResult.setValue(mcBiz);
		return iLookResult;
	}
	
	/**
	 * Description: 获取菜谱设置
	 * @author: Mobile Web Group-lff
	 * @date: 2018年4月27日 上午11:56:08
	 * 
	 * @see com.wuuxiang.i5xforyou.ilook.service.FoodsService#getPho_menu_group(java.lang.String)
	 */
	@Override
	public ILookResult getPho_menu_group(String mcId) {
		//接口出参
		ILookResult iLookResult = new ILookResult();
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ID,MENUID,NAME,SHOWFLG,TIPS FROM PHO_MENU_GROUP WHERE MENUID =");
		sql.append(" (SELECT MENUID FROM (SELECT MENUID FROM PHO_MENU_MCID WHERE MCID = ");
		sql.append(mcId);
		sql.append(" ORDER BY MODIFYTIME DESC) WHERE ROWNUM = 1) ");
		sql.append(" ORDER BY ID ");

		List<MenuGroup> groupList = o2oMenuGroup.getList(sql.toString(), MenuGroup.class);
		
		iLookResult.setApi(ILookResult.DB_SELECT);
		iLookResult.setParams(sql.toString());
		iLookResult.setRst(JSON.toJSONString(groupList));
		iLookResult.setValue(groupList);
		return iLookResult;
	}

	
}
