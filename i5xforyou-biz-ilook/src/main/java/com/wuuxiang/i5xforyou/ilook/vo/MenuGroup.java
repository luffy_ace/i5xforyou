package com.wuuxiang.i5xforyou.ilook.vo;

import java.util.List;

public class MenuGroup {

	private String id;
	private String menuId;
	private String name;
	private String showFlg = "0";//1显示，0不显示
	private String tips;//提示语
	private List<TcClass> tcClassList;//套餐菜谱
	private List<ItemClass> itemClassList;//普通菜谱
	
	//兼容老数据结构的字段
	private String tjDishesName; //等于name
	private String tjDishesFlag = "0";//等于showFlg
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		this.tjDishesName = name;
	}
	public String getShowFlg() {
		return showFlg;
	}
	public void setShowFlg(String showFlg) {
		this.showFlg = showFlg;
		this.tjDishesFlag = showFlg;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	public List<TcClass> getTcClassList() {
		return tcClassList;
	}
	public void setTcClassList(List<TcClass> tcClassList) {
		this.tcClassList = tcClassList;
	}
	public List<ItemClass> getItemClassList() {
		return itemClassList;
	}
	public void setItemClassList(List<ItemClass> itemClassList) {
		this.itemClassList = itemClassList;
	}
	public String getTjDishesName() {
		return tjDishesName;
	}
	public void setTjDishesName(String tjDishesName) {
		this.tjDishesName = tjDishesName;
	}
	public String getTjDishesFlag() {
		return tjDishesFlag;
	}
	public void setTjDishesFlag(String tjDishesFlag) {
		this.tjDishesFlag = tjDishesFlag;
	}
	

	
}
