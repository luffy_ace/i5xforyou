package com.wuuxiang.i5xforyou.ilook;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/** 
 * @ClassName: Test <br/>
 *  <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月19日 下午1:04:01
 *
 */
public class Test {

	public static void main(String[] args) {
	
		JSONObject postData = new JSONObject();
		postData.put("mcID", 111);
		postData.put("tablenumber", 222);
		Map<String, String> params= new HashMap<String, String>();
		params.put("data", postData.toString());
		
		System.out.println(params.toString());
		
	}

}
