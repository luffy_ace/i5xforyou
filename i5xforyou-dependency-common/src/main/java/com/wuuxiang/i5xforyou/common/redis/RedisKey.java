package com.wuuxiang.i5xforyou.common.redis;

/** 
 * @ClassName: RedisKey <br/>
 * redis key <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月14日 下午6:03:46
 *
 */
public class RedisKey {
	
	//crm版本号
	public static final String CRM_VERSION = "i5xforyou.crmversion";
	
	// 短信发送时间戳
    public static final String REDIS_SMSSENDTIME = "i5xforyou.sms.sendtime";
	
}
