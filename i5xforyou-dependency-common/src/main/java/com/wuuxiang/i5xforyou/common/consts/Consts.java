package com.wuuxiang.i5xforyou.common.consts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/** 
 * @ClassName: Consts <br/>
 * 共通变量，对应i5xforyou-conf中的变量 <br/>
 * @author Mobile Web Group-lff
 * @date 2018年3月15日 上午8:39:59
 *
 */
@Component
@RefreshScope
public class Consts {
	/* request的来源header key */
	public static final String API_CALLER = "apiCaller";
	//crm版本
	public static final String CRM6 = "6";
	public static final String CRM7 = "7";
	public static final String CRM_UNKOWN = "UNKOWN";
	/*
	 * 重要，对静态static变量通过@value赋值，要通过非静态的set方法才行
	 */
	/* CRM6的url */
	public static String CRM6_WEBSERVICE_URL;
	/* CRM7的url */
	public static String CRM7_WEBSERVICE_URL;
	/* CRM7的公钥 */
	public static String CRM7_PUBLIC_KEY;
	/* 云端API的url */
	public static String CLOUD_API_URL;

	@Value("${http.crm6.webservice.url:''}")
	public void setCRM6_WEBSERVICE_URL(String cRM6_WEBSERVICE_URL) {
		CRM6_WEBSERVICE_URL = cRM6_WEBSERVICE_URL;
	}
	
	@Value("${http.crm7.webservice.url:''}")
	public void setCRM7_WEBSERVICE_URL(String cRM7_WEBSERVICE_URL) {
		CRM7_WEBSERVICE_URL = cRM7_WEBSERVICE_URL;
	}

	@Value("${http.crm7.webservice.publicKey:''}")
	public void setCRM7_PUBLIC_KEY(String cRM7_PUBLIC_KEY) {
		CRM7_PUBLIC_KEY = cRM7_PUBLIC_KEY;
	}

	@Value("${http.cloud.api.url:''}")
	public void setCLOUD_API_URL(String cLOUD_API_URL) {
		CLOUD_API_URL = cLOUD_API_URL;
	}
	
	
	
}
