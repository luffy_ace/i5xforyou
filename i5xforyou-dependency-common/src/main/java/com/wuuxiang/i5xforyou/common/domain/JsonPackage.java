package com.wuuxiang.i5xforyou.common.domain;

import com.alibaba.fastjson.JSONObject;

public class JsonPackage extends JSONObject {


	/**
	 * @Fields serialVersionUID 
	 */
	private static final long serialVersionUID = -2895226138243589396L;
	//断路由超时
	private static final int HYSTRIX_TIMEOUT = 500;
	
	/**
	 * 构造方法
	 *
	 * @param status
	 * @param message
	 * @param result
	 * @return
	 */
	public JsonPackage() {
		this.put("status", 0);
		this.put("message", "成功");
	}

	public JsonPackage(Integer status, String message) {
		this.put("status", status);
		this.put("message", message);
	}

	public JsonPackage(Integer status, String message, Object result) {
		this.put("status", status);
		this.put("message", message);
		this.put("result", result);
	}

	/**
	 * getHystrixJsonPackage <br/>
	 * 生成断路由返回json <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月15日 下午2:55:45
	 *
	 * @return JsonPackage
	 */
	public static JsonPackage getHystrixJsonPackage() {
		JsonPackage hystrix= new JsonPackage();
		hystrix.setStatus(HYSTRIX_TIMEOUT);
		hystrix.setMessage("服务超时");
		return hystrix;
	}
	
	
	/**
	 * GET AND SET METHODS
	 */
	public Integer getStatus() {
		return this.getInteger("status");
	}

	public void setStatus(Integer status) {
		this.put("status", status);
	}

	public String getMessage() {
		return this.getString("message");
	}

	public void setMessage(String message) {
		this.put("message", message);
	}

	public Object getResult() {
		return this.get("result");
	}

	public void setResult(Object result) {
		this.put("result", result);
	}
}
