package com.wuuxiang.i5xforyou.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.wuuxiang.i5xforyou.common.consts.Consts;

public class BaseClass {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	protected static final String WEI_XIN = "wx";//微信
	protected static final String ZHI_FU_BAO = "zfb";//支付宝
	protected static final String XIAO_CHENG_XU = "xcx";//小程序

	/**
	 * getRequest <br/>
	 * 获取request <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午2:48:30
	 *
	 * @return HttpServletRequest
	 */
	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	/**
	 * getHttpHeader <br/>
	 * 获取header里面key对应的值 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午2:50:29
	 *
	 * @param key
	 * @return String
	 */
	public String getHttpHeader(String key) {
		HttpServletRequest request = getRequest();
		return request.getHeader(key);
	}
	
	/**
	 * getApiCaller <br/>
	 * 获取apiCaller，调用api的来源：微信，支付宝，小程序 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月21日 下午4:48:35
	 *
	 * @return String
	 */
	public String getApiCaller() {
		//apiCaller
		String headerValue = getHttpHeader(Consts.API_CALLER);
		if (WEI_XIN.equals(headerValue)) {
			return WEI_XIN;
		} else if (ZHI_FU_BAO.equals(headerValue)) {
			return ZHI_FU_BAO;
		}else if (XIAO_CHENG_XU.equals(headerValue)) {
			return XIAO_CHENG_XU;
		} else {
			//默认为微信
			return WEI_XIN;
		}
	}
	
	
	/**
	 * getCrm7UrlPrefix <br/>
	 * 返回crm7接口的前缀，区分支付宝和微信接口 <br/>
	 *
	 * @author Mobile Web Group-lff
	 * @date 2018年3月16日 下午3:02:19
	 *
	 * @return String
	 */
	public String getCrm7UrlPrefix() {
		//apiCaller
		String apiCaller = getApiCaller();
		if (ZHI_FU_BAO.equals(apiCaller)) {
			//支付宝
			return "crm7alipay/";
		} else {
			//其它情况
			return "crm7wxapi/";
		}
	}
	
	
}
